﻿
namespace PrintingIDCard
{
    partial class FormParticipantOfficialCabor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmb_cabor = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.cmb_kontingen = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Foto = new System.Windows.Forms.DataGridViewImageColumn();
            this.QRCode = new System.Windows.Forms.DataGridViewImageColumn();
            this.cmbFunction = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbCabor2 = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // cmb_cabor
            // 
            this.cmb_cabor.FormattingEnabled = true;
            this.cmb_cabor.Location = new System.Drawing.Point(168, 43);
            this.cmb_cabor.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_cabor.Name = "cmb_cabor";
            this.cmb_cabor.Size = new System.Drawing.Size(228, 24);
            this.cmb_cabor.TabIndex = 34;
            this.cmb_cabor.SelectedIndexChanged += new System.EventHandler(this.cmb_cabor_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 46);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 17);
            this.label2.TabIndex = 35;
            this.label2.Text = "Pilih Cabang Olahraga";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(168, 110);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 29;
            this.button1.Text = "Tampilkan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 17);
            this.label1.TabIndex = 28;
            this.label1.Text = "Pilih Kontingen";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(276, 110);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 31;
            this.button2.Text = "Cetak";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmb_kontingen
            // 
            this.cmb_kontingen.FormattingEnabled = true;
            this.cmb_kontingen.Location = new System.Drawing.Point(168, 12);
            this.cmb_kontingen.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_kontingen.Name = "cmb_kontingen";
            this.cmb_kontingen.Size = new System.Drawing.Size(228, 24);
            this.cmb_kontingen.TabIndex = 27;
            this.cmb_kontingen.SelectedIndexChanged += new System.EventHandler(this.cmb_kontingen_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Foto,
            this.QRCode});
            this.dataGridView1.Location = new System.Drawing.Point(13, 151);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 80;
            this.dataGridView1.Size = new System.Drawing.Size(1414, 618);
            this.dataGridView1.TabIndex = 30;
            // 
            // Foto
            // 
            this.Foto.DataPropertyName = "Foto";
            this.Foto.HeaderText = "Foto";
            this.Foto.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Foto.MinimumWidth = 6;
            this.Foto.Name = "Foto";
            this.Foto.ReadOnly = true;
            this.Foto.Width = 125;
            // 
            // QRCode
            // 
            this.QRCode.DataPropertyName = "QRCode";
            this.QRCode.HeaderText = "QRCode";
            this.QRCode.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.QRCode.MinimumWidth = 6;
            this.QRCode.Name = "QRCode";
            this.QRCode.ReadOnly = true;
            this.QRCode.Width = 125;
            // 
            // cmbFunction
            // 
            this.cmbFunction.FormattingEnabled = true;
            this.cmbFunction.Items.AddRange(new object[] {
            "Fo",
            "Fox",
            "Fxx"});
            this.cmbFunction.Location = new System.Drawing.Point(168, 74);
            this.cmbFunction.Margin = new System.Windows.Forms.Padding(4);
            this.cmbFunction.Name = "cmbFunction";
            this.cmbFunction.Size = new System.Drawing.Size(228, 24);
            this.cmbFunction.TabIndex = 36;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 77);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 17);
            this.label3.TabIndex = 37;
            this.label3.Text = "Pilih Fungsi";
            // 
            // cmbCabor2
            // 
            this.cmbCabor2.FormattingEnabled = true;
            this.cmbCabor2.Location = new System.Drawing.Point(404, 43);
            this.cmbCabor2.Margin = new System.Windows.Forms.Padding(4);
            this.cmbCabor2.Name = "cmbCabor2";
            this.cmbCabor2.Size = new System.Drawing.Size(228, 24);
            this.cmbCabor2.TabIndex = 38;
            this.cmbCabor2.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(384, 110);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(171, 28);
            this.button3.TabIndex = 39;
            this.button3.Text = "Generate QR Code";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(563, 110);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(171, 28);
            this.button4.TabIndex = 40;
            this.button4.Text = "Reset QR Code";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // FormParticipantOfficialCabor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1440, 782);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.cmbCabor2);
            this.Controls.Add(this.cmbFunction);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmb_cabor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.cmb_kontingen);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FormParticipantOfficialCabor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormParticipant";
            this.Load += new System.EventHandler(this.FormParticipant_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cmb_cabor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox cmb_kontingen;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewImageColumn Foto;
        private System.Windows.Forms.DataGridViewImageColumn QRCode;
        private System.Windows.Forms.ComboBox cmbFunction;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbCabor2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}