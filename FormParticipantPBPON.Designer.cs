﻿
namespace PrintingIDCard
{
    partial class FormParticipantPBPON
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Foto = new System.Windows.Forms.DataGridViewImageColumn();
            this.QRCode = new System.Windows.Forms.DataGridViewImageColumn();
            this.btnCetakVIP = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbBidang = new System.Windows.Forms.ComboBox();
            this.btnCetakA = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbSubBidang = new System.Windows.Forms.ComboBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.ckAjudan = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(167, 88);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 29;
            this.button1.Text = "Tampilkan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Foto,
            this.QRCode});
            this.dataGridView1.Location = new System.Drawing.Point(13, 136);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 80;
            this.dataGridView1.Size = new System.Drawing.Size(1414, 605);
            this.dataGridView1.TabIndex = 30;
            // 
            // Foto
            // 
            this.Foto.DataPropertyName = "Foto";
            this.Foto.HeaderText = "Foto";
            this.Foto.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Foto.MinimumWidth = 6;
            this.Foto.Name = "Foto";
            this.Foto.ReadOnly = true;
            this.Foto.Width = 125;
            // 
            // QRCode
            // 
            this.QRCode.DataPropertyName = "QRCode";
            this.QRCode.HeaderText = "QRCode";
            this.QRCode.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.QRCode.MinimumWidth = 6;
            this.QRCode.Name = "QRCode";
            this.QRCode.ReadOnly = true;
            this.QRCode.Width = 125;
            // 
            // btnCetakVIP
            // 
            this.btnCetakVIP.Location = new System.Drawing.Point(417, 88);
            this.btnCetakVIP.Margin = new System.Windows.Forms.Padding(4);
            this.btnCetakVIP.Name = "btnCetakVIP";
            this.btnCetakVIP.Size = new System.Drawing.Size(134, 28);
            this.btnCetakVIP.TabIndex = 34;
            this.btnCetakVIP.Text = "Cetak Kode B2";
            this.btnCetakVIP.UseVisualStyleBackColor = true;
            this.btnCetakVIP.Click += new System.EventHandler(this.btnCetakVIP_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 36;
            this.label2.Text = "Pilih Bidang";
            // 
            // cmbBidang
            // 
            this.cmbBidang.FormattingEnabled = true;
            this.cmbBidang.Items.AddRange(new object[] {
            "DPD RI",
            "DPR RI",
            "BPK RI",
            "MPR RI",
            "MK",
            "Kejagung"});
            this.cmbBidang.Location = new System.Drawing.Point(167, 17);
            this.cmbBidang.Margin = new System.Windows.Forms.Padding(4);
            this.cmbBidang.Name = "cmbBidang";
            this.cmbBidang.Size = new System.Drawing.Size(228, 24);
            this.cmbBidang.TabIndex = 35;
            this.cmbBidang.SelectedIndexChanged += new System.EventHandler(this.cmbBidang_SelectedIndexChanged);
            // 
            // btnCetakA
            // 
            this.btnCetakA.Location = new System.Drawing.Point(559, 88);
            this.btnCetakA.Margin = new System.Windows.Forms.Padding(4);
            this.btnCetakA.Name = "btnCetakA";
            this.btnCetakA.Size = new System.Drawing.Size(134, 28);
            this.btnCetakA.TabIndex = 37;
            this.btnCetakA.Text = "Cetak Kode B3";
            this.btnCetakA.UseVisualStyleBackColor = true;
            this.btnCetakA.Click += new System.EventHandler(this.btnCetakA_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1169, 88);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(171, 28);
            this.button4.TabIndex = 42;
            this.button4.Text = "Reset QR Code";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(990, 88);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(171, 28);
            this.button3.TabIndex = 41;
            this.button3.Text = "Generate QR Code";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(701, 88);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(134, 28);
            this.button2.TabIndex = 49;
            this.button2.Text = "Cetak Kode B4";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(275, 88);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(134, 28);
            this.button5.TabIndex = 50;
            this.button5.Text = "Cetak Kode B1";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 52);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 17);
            this.label3.TabIndex = 54;
            this.label3.Text = "Pilih Sub Bidang";
            // 
            // cmbSubBidang
            // 
            this.cmbSubBidang.FormattingEnabled = true;
            this.cmbSubBidang.Location = new System.Drawing.Point(167, 49);
            this.cmbSubBidang.Margin = new System.Windows.Forms.Padding(4);
            this.cmbSubBidang.Name = "cmbSubBidang";
            this.cmbSubBidang.Size = new System.Drawing.Size(228, 24);
            this.cmbSubBidang.TabIndex = 53;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(843, 88);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(134, 28);
            this.button6.TabIndex = 55;
            this.button6.Text = "Cetak Kode A";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(417, 52);
            this.button7.Margin = new System.Windows.Forms.Padding(4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(134, 28);
            this.button7.TabIndex = 56;
            this.button7.Text = "Cetak VIP1";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(559, 52);
            this.button8.Margin = new System.Windows.Forms.Padding(4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(134, 28);
            this.button8.TabIndex = 57;
            this.button8.Text = "Cetak VIP2";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(701, 52);
            this.button9.Margin = new System.Windows.Forms.Padding(4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(134, 28);
            this.button9.TabIndex = 58;
            this.button9.Text = "Cetak VIP3";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // ckAjudan
            // 
            this.ckAjudan.AutoSize = true;
            this.ckAjudan.Location = new System.Drawing.Point(417, 20);
            this.ckAjudan.Name = "ckAjudan";
            this.ckAjudan.Size = new System.Drawing.Size(265, 21);
            this.ckAjudan.TabIndex = 59;
            this.ckAjudan.Text = "Cek Jika Data Peserta adalah Ajudan";
            this.ckAjudan.UseVisualStyleBackColor = true;
            // 
            // FormParticipantPBPON
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1440, 782);
            this.Controls.Add(this.ckAjudan);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbSubBidang);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnCetakA);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbBidang);
            this.Controls.Add(this.btnCetakVIP);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FormParticipantPBPON";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormParticipant";
            this.Load += new System.EventHandler(this.FormParticipant_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewImageColumn Foto;
        private System.Windows.Forms.DataGridViewImageColumn QRCode;
        private System.Windows.Forms.Button btnCetakVIP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbBidang;
        private System.Windows.Forms.Button btnCetakA;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbSubBidang;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.CheckBox ckAjudan;
    }
}