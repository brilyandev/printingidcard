﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Net;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace PrintingIDCard
{
    public partial class FormParticipantVIPKontingen : Form
    {
        public FormParticipantVIPKontingen()
        {
            InitializeComponent();
        }

        public Boolean Ajudan = false;

        public String DataHeaderCabor(string Alamat, string IdKontingen)
        {
            string Data = "";
            var request = (HttpWebRequest)WebRequest.Create(Alamat);
            request.Method = "POST";
            request.Headers.Add("x-access-token", PublicData.token);
            request.ContentType = "application/x-www-form-urlencoded";
            using (var streamWriter = new System.IO.StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write("contingent_id=" + IdKontingen , System.Text.Encoding.UTF8);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var response = (HttpWebResponse)request.GetResponse();
            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
            {
                Data = reader.ReadToEnd();
            }
            return Data;
        }

        
        public String DataHeader(string Address)
        {
            string Data = "";
            var request = (HttpWebRequest)WebRequest.Create(Address);
            request.Method = "GET";
            request.Headers.Add("x-access-token", PublicData.token);
            var response = (HttpWebResponse)request.GetResponse();
            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
            {
                Data = reader.ReadToEnd();
            }
            return Data;
        }

        private void Bind_ComboBox(DataTable Rows, ComboBox cmb)
        {
            cmb.Items.Clear();
            cmb.Items.Add("--PILIH--");
            int _i = 1;
            IdKon[0] = "0";
            foreach (DataRow Row in Rows.Select())
            {
                cmb.Items.Add(Row[1].ToString());
                IdKon[_i] = Row[0].ToString();
                _i++;
            }
        }

        private void Bind_ComboBox2(DataTable Rows, ComboBox cmb)
        {
            cmb.Items.Clear();
            cmb.Items.Add("--PILIH--");
            int _i = 1;
            IdKon2[0] = "0";
            foreach (DataRow Row in Rows.Select())
            {
                cmb.Items.Add(Row[1].ToString());
                IdKon2[_i] = Row[0].ToString();
                _i++;
            }
        }

        private void Bind_ComboBox3(DataTable Rows, ComboBox cmb)
        {
            cmb.Items.Clear();
            cmb.Items.Add("--PILIH--");
            int _i = 1;
            IdKon3[0] = "0";
            foreach (DataRow Row in Rows.Select())
            {
                cmb.Items.Add(Row[1].ToString());
                IdKon3[_i] = Row[0].ToString();
                _i++;
            }
        }

        private void Bind_ComboBoxCabor(DataTable Rows, ComboBox cmb)
        {
            cmb.Items.Clear();
            cmb.Items.Add("--PILIH--");
            int _i = 1;
            IdCabor[0] = "0";
            foreach (DataRow Row in Rows.Select())
            {
                cmb.Items.Add(Row[1].ToString());
                IdCabor[_i] = Row[0].ToString();
                _i++;
            }
        }

        string[] IdKon = new string[70];
        string[] IdKon2 = new string[70];
        string[] IdKon3 = new string[70];
        string[] IdCabor = new string[70];

        string IdKontingen_list = "";

        private  DataTable dataKontingen()
        {
            JArray o = JArray.Parse(DataHeader(PublicData.url+"/private/contingents"));
            DataTable dt = new DataTable();
            dt.Columns.Add("_id");
            dt.Columns.Add("name");
            for (int i = 0; i < o.Count; i++)
            {
                string _id = "", name = "";
                try
                { _id = ((JValue)o[i]["_id"]).ToString(); }
                catch { }
                try
                { name = ((JValue)o[i]["name"]).ToString(); }
                catch { }
                dt.Rows.Add(_id, name);
            }
            return dt;
        }

        private DataTable dataKontingenCode()
        {
            JArray o = JArray.Parse(DataHeader(PublicData.url + "/private/contingents"));
            DataTable dt = new DataTable();
            dt.Columns.Add("_id");
            dt.Columns.Add("initial");
            for (int i = 0; i < o.Count; i++)
            {
                string _id = "", initial = "";
                try
                { _id = ((JValue)o[i]["initial"]).ToString(); }
                catch { }
                try
                { initial = ((JValue)o[i]["initial"]).ToString(); }
                catch { }
                dt.Rows.Add(_id, initial);
            }
            return dt;
        }

        private DataTable dataCabor()
        {
            JArray o = JArray.Parse(DataHeader(PublicData.url+"/private/events"));
            DataTable dt = new DataTable();
            dt.Columns.Add("_id");
            dt.Columns.Add("event_name");
            for (int i = 0; i < o.Count; i++)
            {
                string _id = "", event_name = "";
                try
                { _id = ((JValue)o[i]["_id"]).ToString(); }
                catch { }
                try
                { event_name = ((JValue)o[i]["event_name"]).ToString(); }
                catch { }
                dt.Rows.Add(_id, event_name);
            }
            return dt;
        }

        private DataTable dataCaborByContingent(string IdKontingen)
        {
            JArray o = JArray.Parse(DataHeaderCabor(PublicData.url+"/private/participants/event_list", IdKontingen));
            DataTable dt = new DataTable();
            dt.Columns.Add("_id");
            dt.Columns.Add("event_name");
            for (int i = 0; i < o.Count; i++)
            {
                string _id = "", event_name = "";
                try
                { _id = ((JValue)o[i]["event_id"]).ToString(); }
                catch { }
                try
                { event_name = ((JValue)o[i]["event_name"]).ToString(); }
                catch { }
                dt.Rows.Add(_id, event_name);
            }
            return dt;
        }

        private void FormParticipant_Load(object sender, EventArgs e)
        {
            Bind_ComboBox(dataKontingen(), cmb_kontingen);
            Bind_ComboBox2(dataKontingenCode(), comboBox1);
            //dataKontingenCode();
            cmb_kontingen.SelectedIndex = 0;
            comboBox1.SelectedIndex = 0;
            cmbjabatan.SelectedIndex = 0;
            //button1.Enabled = false;
            //button2.Enabled = false;


            //JArray o = JArray.Parse(DataHeader("https://apigames.ponxx2020papua.com/private/participants/5dca4fd54b46520adac7c3ce/5db9032070e8b43374e09764/Athlete"));
            ////MessageBox.Show(o.Count.ToString());
            //DataTable dt = new DataTable();
            //dt.Columns.Add("full_name");
            //dt.Columns.Add("gender");
            //dt.Columns.Add("participant_type");
            //dt.Columns.Add("contingent_name");
            //dt.Columns.Add("event_name");
            //dt.Columns.Add("photo");
            //dt.Columns.Add("personelID");
            //dt.Columns.Add("Status Cetak");
            //dt.Columns.Add("Tgl Cetak");
            //dt.Columns.Add("Petugas");
            //dt.Columns.Add("Aksi");

            //for (int i = 0; i < o.Count; i++)
            //{
            //    string full_name = "", gender = "", participant_type = "", contingent_name = "", event_name = "", photo = "", personelID = "";
            //    try
            //    { full_name = ((JValue)o[i]["full_name"]).ToString(); }
            //    catch { }
            //    try { gender = ((JValue)o[i]["gender"]).ToString(); } catch { }
            //    try { participant_type = ((JValue)o[i]["participant_type"]).ToString(); } catch { }
            //    try
            //    {
            //        if ((JValue)o[i]["contingent_name"] != null)
            //            contingent_name = ((JValue)o[i]["contingent_name"]).ToString();
            //    }
            //    catch { }
            //    try
            //    {
            //        if ((JValue)o[i]["event_name"] != null)
            //            event_name = ((JValue)o[i]["event_name"]).ToString();
            //    }
            //    catch { }
            //    try { photo = ((JValue)o[i]["photo"]).ToString(); } catch { }
            //    try
            //    {
            //        if ((JValue)o[i]["personelID"] != null)
            //            personelID = ((JValue)o[i]["personelID"]).ToString();
            //    }
            //    catch { }
            //    dt.Rows.Add(full_name,
            //         gender,
            //         participant_type,
            //        contingent_name,
            //         event_name,
            //        photo,
            //          personelID
            //   );

            //}
            //dataGridView1.DataSource = null;

            //dataGridView1.Columns.Clear();

            //dataGridView1.DataSource = dt;
        }

        private DataSet dataParticipant()
        {
            string url = PublicData.url + "/private/participants?contingent_id=" + IdKon[cmb_kontingen.SelectedIndex]+"&participant_type="+ cmbjabatan.SelectedItem.ToString();
            JArray o = JArray.Parse(DataHeader(url));
            DataTable dt = new DataTable("Atlit");
            dt.Columns.Add("_id");
            dt.Columns.Add("photo");
            dt.Columns.Add("Register_Number");
            dt.Columns.Add("NamaLengkap");
            dt.Columns.Add("JenisPeserta");;
            dt.Columns.Add("Kontingen");
            dt.Columns.Add("Cabor");
            dt.Columns.Add("printing_status");
            dt.Columns.Add("KodeAkses");           
            dt.Columns.Add("KodeParticipant");
            dt.Columns.Add("NIK");

            for (int i = 0; i < o.Count; i++)
            {
                string _id="", NamaLengkap="", JenisPeserta="",
                    Kontingen="", Cabor="", photo = "", Register_Number = "",
                    printing_status = "Belum Tercetak", KodeAkses = "F", KodeParticipant="", NIK = "";
                try
                {
                    _id = ((JValue)o[i]["_id"]).ToString();
                }
                catch { }
                try
                { 
                    NamaLengkap = ((JValue)o[i]["full_name"]).ToString(); }
                catch { }
                try
                {
                    NIK = ((JValue)o[i]["ID_Number"]).ToString();
                }
                catch { }
                try { JenisPeserta = ((JValue)o[i]["jobposition"]).ToString();
                } catch { }
                try
                {
                    //if ((JValue)o[i]["contingent_name"] != null)
                    //{
                        Kontingen = cmb_kontingen.SelectedItem.ToString();// ((JValue)o[i]["contingent_name"]).ToString();
                    //}
                        
                }
                catch { }
                try { photo = ((JValue)o[i]["photo"]).ToString(); } catch { }
                
                try
                {
                    if ((JValue)o[i]["Register_Number"] != null)
                    {
                        Register_Number = ((JValue)o[i]["Register_Number"]).ToString();
                        KodeParticipant = ((JValue)o[i]["Register_Number"]).ToString();
                    }
                }
                catch { }
                try
                {
                    if ((JValue)o[i]["printing_status"] != null)
                    {
                        if (Convert.ToBoolean((JValue)o[i]["printing_status"]) == true)
                        {
                            printing_status = "Tercetak";
                        }
                        else
                        {
                            printing_status = "Belum Tercetak";
                        }
                    }

                }
                catch { }
                dt.Rows.Add(
                    _id,
                    photo,
                    Register_Number,
                    NamaLengkap,                    
                    JenisPeserta,                    
                    Kontingen,
                    Cabor,                    
                    printing_status,
                    KodeAkses,
                    KodeParticipant,
                    NIK
               );

            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return ds;
        }

        private DataTable registermultidiscipline(string _ID_Number, string _Participant_Type)
        {
            DataTable dt = new DataTable("MultiDisipline");
            try
            {
                var request = (HttpWebRequest)WebRequest.Create("https://apigames.ponxx2020papua.com/participants/multidiscipline");
                request.Method = "POST";
                request.Headers.Add("x-access-token", PublicData.token);
                request.ContentType = "application/x-www-form-urlencoded";
                using (var streamWriter = new System.IO.StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write("ID_Number="+ _ID_Number + "&participant_type="+ _Participant_Type, System.Text.Encoding.UTF8);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var response = (HttpWebResponse)request.GetResponse();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
                {
                    JArray Data = JArray.Parse(reader.ReadToEnd());
                    
                    try
                    {                        
                        dt.Columns.Add("_id");
                        dt.Columns.Add("Register_Number");
                        dt.Columns.Add("full_name");
                        dt.Columns.Add("contingent_name"); ;
                        dt.Columns.Add("event_name");
                        dt.Columns.Add("event_code");

                        for (int i = 0; i < Data.Count; i++)
                        {
                            string _id = "", Register_Number = "", full_name = "",
                                contingent_name = "", event_name = "", event_code="";
                            try{ _id = ((JValue)Data[i]["_id"]).ToString();}catch { }
                            try { Register_Number = ((JValue)Data[i]["Register_Number"]).ToString(); } catch { }
                            try { full_name = ((JValue)Data[i]["full_name"]).ToString(); } catch { }
                            try { contingent_name = ((JValue)Data[i]["contingent_name"]).ToString(); } catch { }
                            try { event_name = ((JValue)Data[i]["event_name"]).ToString(); } catch { }
                            try { event_code = ((JValue)Data[i]["event_code"]).ToString(); } catch { }
                            dt.Rows.Add(
                                _id,
                                Register_Number,
                                full_name,
                                contingent_name,
                                event_name,
                                event_code
                           );

                        }
                        
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Username dan password tidak tersedia", ex.Message);
                    }

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Silahkan periksa koneksi internet kamu", ex.Message);
            }
            return dt;
        }


        private void button2_Click(object sender, EventArgs e)
        {
            PrintIDCard("A", ckAjudan.Checked);
        }

        private void LoadImage(DataRow objDataRow, string strImageField, string Foto)
        {
            try
            {
                System.Net.WebClient web = new System.Net.WebClient();
                byte[] Image = null;
                Image = web.DownloadData(Foto);
                objDataRow[strImageField] = Image;
            }
            catch
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void AddImageColumn(DataTable objDataTable, string strFieldName)
        {
            try
            {
                DataColumn objDataColumn = new DataColumn(strFieldName, Type.GetType("System.Byte[]"));
                objDataTable.Columns.Add(objDataColumn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public DataSet ds = new DataSet();
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmb_kontingen.SelectedIndex != 0)
                {
                    IdKontingen_list = IdKontingen_list + "'" + IdKon[cmb_kontingen.SelectedIndex] + "',";
                }                
                if (IdKontingen_list.Length > 0)
                {
                    IdKontingen_list = IdKontingen_list + "''";
                }
                ds = dataParticipant();

                AddImageColumn(ds.Tables["Atlit"], "Foto");
                AddImageColumn(ds.Tables["Atlit"], "QRCode");
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    LoadImage(ds.Tables[0].Rows[index], "Foto", "https://apigames.ponxx2020papua.com/repo/participant/" + ds.Tables[0].Rows[index]["photo"].ToString());
                    LoadImage(ds.Tables[0].Rows[index], "QRCode", PublicData.url+"/qr/" + ds.Tables[0].Rows[index]["Register_Number"].ToString());
                }
                    //ds.Tables["Atlit"].Columns.Add(new DataColumn("Cetak", typeof(System.Windows.Forms.Button)));
                    
                dataGridView1.DataSource = ds.Tables[0];//q_data.dtIDCardAtlit(IdKejuaraan, IdKontingen_list,Provinsi,  "").Tables[0];

                int _i = 0;
                dataGridView1.Columns["_id"].Visible = false;
                dataGridView1.Columns["photo"].Visible = false;
                dataGridView1.Columns["Register_Number"].Visible = false;
                dataGridView1.Columns["KodeAkses"].Visible = false;
                dataGridView1.Columns["KodeParticipant"].Visible = false;
                dataGridView1.Columns["NIK"].Visible = false;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells["printing_status"].Value.ToString() == "Tercetak")
                    {
                        dataGridView1.Rows[_i].DefaultCellStyle.BackColor = Color.Green;
                        dataGridView1.Rows[_i].DefaultCellStyle.ForeColor = Color.White;
                    }
                    _i++;
                }
                MessageBox.Show("Data Bind Selesai");
                btnCetakA.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmb_kontingen_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            PrintIDCard("B", ckAjudan.Checked);
        }

        private void cmbjabatan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbjabatan.SelectedIndex == 0)
            {
                btnCetakVIP.Visible = true;
                btnCetakA.Visible = false;
                btnKodeA2.Visible = false;
                btnCetakB.Visible = false;
                btnKodeB2.Visible = false;
                btnKodeB3.Visible = false;
            }
            else
            {
                btnCetakVIP.Visible = false;
                btnCetakA.Visible = true;
                btnKodeA2.Visible = true;
                btnCetakB.Visible = true;
                btnKodeB2.Visible = true;
                btnKodeB3.Visible = true;
            }
        }

        private void btnCetakVIP_Click(object sender, EventArgs e)
        {
            PrintIDCard("VIP", ckAjudan.Checked);
        }

        void PrintIDCard(string Kode, Boolean Ajudan)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                FormPreviewPrintVIPKontingen frm = new FormPreviewPrintVIPKontingen();
                frm.lblParticipantID.Text = dataGridView1.Rows[row.Index].Cells["_id"].Value.ToString().ToUpper();
                frm.lblfullname.Text = dataGridView1.Rows[row.Index].Cells["NamaLengkap"].Value.ToString().ToUpper();
                frm.lblcontingent_name.Text = dataGridView1.Rows[row.Index].Cells["Kontingen"].Value.ToString().ToUpper();

                var dataevent = dataKontingenCode();
                var index = 0;
                frm.lblevent_name.Text = dataGridView1.Rows[row.Index].Cells["Kontingen"].Value.ToString().ToUpper();
                frm.lblevent_code.Text = IdKon2[cmb_kontingen.SelectedIndex].ToUpper();
                frm.photo = dataGridView1.Rows[row.Index].Cells["photo"].Value.ToString();
                frm.QRCode = dataGridView1.Rows[row.Index].Cells["Register_Number"].Value.ToString();
                //frm.lblevent_name.Text = eventlist;//dataGridView1.Rows[row.Index].Cells["Cabor"].Value.ToString();
                frm.lblKodeParticipant.Text = dataGridView1.Rows[row.Index].Cells["Register_Number"].Value.ToString();
                frm.lblJenisPeserta.Text = dataGridView1.Rows[row.Index].Cells["JenisPeserta"].Value.ToString().ToUpper();
                frm.lblNIK.Text = dataGridView1.Rows[row.Index].Cells["NIK"].Value.ToString();

                frm.lblKodeAkses.Text = Kode;//dataGridView1.Rows[row.Index].Cells["KodeAkses"].Value.ToString();
                frm.Ajudan = Ajudan;
                var requestPhoto = WebRequest.Create(PublicData.url + "/repo/participant/" + dataGridView1.Rows[row.Index].Cells["photo"].Value.ToString());
                using (var response = requestPhoto.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    frm.pbPhoto.Image = Bitmap.FromStream(stream);
                }
                var requestQRCode = WebRequest.Create(PublicData.url + "/qr/" + dataGridView1.Rows[row.Index].Cells["Register_Number"].Value.ToString());
                using (var response = requestQRCode.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    frm.pbQRCode.Image = Bitmap.FromStream(stream);
                }
                frm.ShowDialog();
            }
        }

        private void btnKodeA2_Click(object sender, EventArgs e)
        {
            PrintIDCard("A2", ckAjudan.Checked);
        }

        private void btnKodeB2_Click(object sender, EventArgs e)
        {
            PrintIDCard("B2", ckAjudan.Checked);
        }

        private void btnKodeB3_Click(object sender, EventArgs e)
        {
            PrintIDCard("B3", ckAjudan.Checked);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            PrintIDCard("B4", ckAjudan.Checked);
        }
    }
}
