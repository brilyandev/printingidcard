﻿
namespace PrintingIDCard
{
    partial class FormPreviewPrintOfficialKontingen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbPhoto = new System.Windows.Forms.PictureBox();
            this.lblfullname = new System.Windows.Forms.Label();
            this.lblcontingent_name = new System.Windows.Forms.Label();
            this.lblKodeParticipant = new System.Windows.Forms.Label();
            this.pbQRCode = new System.Windows.Forms.PictureBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnUpdatPrint = new System.Windows.Forms.Button();
            this.btnResetStatus = new System.Windows.Forms.Button();
            this.lblKodeAkses = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblJenisPeserta = new System.Windows.Forms.Label();
            this.lblNIK = new System.Windows.Forms.Label();
            this.btnTutup = new System.Windows.Forms.Button();
            this.lblParticipantID = new System.Windows.Forms.Label();
            this.listEvent = new System.Windows.Forms.DataGridView();
            this.lblevent_code = new System.Windows.Forms.Label();
            this.lblevent_name = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbQRCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listEvent)).BeginInit();
            this.SuspendLayout();
            // 
            // pbPhoto
            // 
            this.pbPhoto.Location = new System.Drawing.Point(12, 77);
            this.pbPhoto.Name = "pbPhoto";
            this.pbPhoto.Size = new System.Drawing.Size(251, 257);
            this.pbPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPhoto.TabIndex = 0;
            this.pbPhoto.TabStop = false;
            // 
            // lblfullname
            // 
            this.lblfullname.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfullname.Location = new System.Drawing.Point(274, 77);
            this.lblfullname.Name = "lblfullname";
            this.lblfullname.Size = new System.Drawing.Size(538, 49);
            this.lblfullname.TabIndex = 1;
            this.lblfullname.Text = "FullName";
            this.lblfullname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblcontingent_name
            // 
            this.lblcontingent_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcontingent_name.Location = new System.Drawing.Point(280, 126);
            this.lblcontingent_name.Name = "lblcontingent_name";
            this.lblcontingent_name.Size = new System.Drawing.Size(534, 31);
            this.lblcontingent_name.TabIndex = 2;
            this.lblcontingent_name.Text = "ContingentName";
            // 
            // lblKodeParticipant
            // 
            this.lblKodeParticipant.Location = new System.Drawing.Point(395, 312);
            this.lblKodeParticipant.Name = "lblKodeParticipant";
            this.lblKodeParticipant.Size = new System.Drawing.Size(244, 22);
            this.lblKodeParticipant.TabIndex = 4;
            this.lblKodeParticipant.Text = "kodeparticipant";
            // 
            // pbQRCode
            // 
            this.pbQRCode.Location = new System.Drawing.Point(312, 263);
            this.pbQRCode.Name = "pbQRCode";
            this.pbQRCode.Size = new System.Drawing.Size(75, 71);
            this.pbQRCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbQRCode.TabIndex = 5;
            this.pbQRCode.TabStop = false;
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(298, 348);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(123, 52);
            this.btnPrint.TabIndex = 6;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnUpdatPrint
            // 
            this.btnUpdatPrint.Location = new System.Drawing.Point(427, 347);
            this.btnUpdatPrint.Name = "btnUpdatPrint";
            this.btnUpdatPrint.Size = new System.Drawing.Size(123, 52);
            this.btnUpdatPrint.TabIndex = 7;
            this.btnUpdatPrint.Text = "Update Status";
            this.btnUpdatPrint.UseVisualStyleBackColor = true;
            this.btnUpdatPrint.Click += new System.EventHandler(this.btnUpdatPrint_Click);
            // 
            // btnResetStatus
            // 
            this.btnResetStatus.Location = new System.Drawing.Point(556, 348);
            this.btnResetStatus.Name = "btnResetStatus";
            this.btnResetStatus.Size = new System.Drawing.Size(123, 52);
            this.btnResetStatus.TabIndex = 8;
            this.btnResetStatus.Text = "Reset Status";
            this.btnResetStatus.UseVisualStyleBackColor = true;
            this.btnResetStatus.Click += new System.EventHandler(this.btnResetStatus_Click);
            // 
            // lblKodeAkses
            // 
            this.lblKodeAkses.BackColor = System.Drawing.Color.Green;
            this.lblKodeAkses.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKodeAkses.ForeColor = System.Drawing.Color.White;
            this.lblKodeAkses.Location = new System.Drawing.Point(393, 263);
            this.lblKodeAkses.Name = "lblKodeAkses";
            this.lblKodeAkses.Size = new System.Drawing.Size(384, 49);
            this.lblKodeAkses.TabIndex = 9;
            this.lblKodeAkses.Text = "KodeAkses";
            this.lblKodeAkses.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(796, 67);
            this.label1.TabIndex = 10;
            this.label1.Text = "Panel Cetak ID Card SIM PON XX 2021 Papua";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJenisPeserta
            // 
            this.lblJenisPeserta.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJenisPeserta.Location = new System.Drawing.Point(7, 356);
            this.lblJenisPeserta.Name = "lblJenisPeserta";
            this.lblJenisPeserta.Size = new System.Drawing.Size(267, 31);
            this.lblJenisPeserta.TabIndex = 11;
            this.lblJenisPeserta.Text = "Jenis Peserta";
            this.lblJenisPeserta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNIK
            // 
            this.lblNIK.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNIK.Location = new System.Drawing.Point(7, 387);
            this.lblNIK.Name = "lblNIK";
            this.lblNIK.Size = new System.Drawing.Size(267, 31);
            this.lblNIK.TabIndex = 12;
            this.lblNIK.Text = "NIK";
            this.lblNIK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnTutup
            // 
            this.btnTutup.Location = new System.Drawing.Point(685, 347);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(123, 52);
            this.btnTutup.TabIndex = 13;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            this.btnTutup.Click += new System.EventHandler(this.btnTutup_Click);
            // 
            // lblParticipantID
            // 
            this.lblParticipantID.Location = new System.Drawing.Point(533, 312);
            this.lblParticipantID.Name = "lblParticipantID";
            this.lblParticipantID.Size = new System.Drawing.Size(244, 22);
            this.lblParticipantID.TabIndex = 14;
            this.lblParticipantID.Text = "participant_id";
            // 
            // listEvent
            // 
            this.listEvent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listEvent.Location = new System.Drawing.Point(12, 430);
            this.listEvent.Name = "listEvent";
            this.listEvent.RowHeadersWidth = 51;
            this.listEvent.RowTemplate.Height = 24;
            this.listEvent.Size = new System.Drawing.Size(796, 150);
            this.listEvent.TabIndex = 15;
            // 
            // lblevent_code
            // 
            this.lblevent_code.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblevent_code.Location = new System.Drawing.Point(276, 166);
            this.lblevent_code.Name = "lblevent_code";
            this.lblevent_code.Size = new System.Drawing.Size(72, 32);
            this.lblevent_code.TabIndex = 18;
            this.lblevent_code.Text = "KODE";
            this.lblevent_code.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblevent_name
            // 
            this.lblevent_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblevent_name.Location = new System.Drawing.Point(347, 166);
            this.lblevent_name.Name = "lblevent_name";
            this.lblevent_name.Size = new System.Drawing.Size(467, 32);
            this.lblevent_name.TabIndex = 3;
            this.lblevent_name.Text = "EventName";
            this.lblevent_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblevent_name.Click += new System.EventHandler(this.label3_Click);
            // 
            // FormPreviewPrintVIPKontingen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(820, 589);
            this.Controls.Add(this.lblevent_code);
            this.Controls.Add(this.listEvent);
            this.Controls.Add(this.lblParticipantID);
            this.Controls.Add(this.btnTutup);
            this.Controls.Add(this.lblNIK);
            this.Controls.Add(this.lblJenisPeserta);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblKodeAkses);
            this.Controls.Add(this.btnResetStatus);
            this.Controls.Add(this.btnUpdatPrint);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.pbQRCode);
            this.Controls.Add(this.lblKodeParticipant);
            this.Controls.Add(this.lblevent_name);
            this.Controls.Add(this.lblcontingent_name);
            this.Controls.Add(this.lblfullname);
            this.Controls.Add(this.pbPhoto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormPreviewPrintVIPKontingen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormPreviewPrint";
            ((System.ComponentModel.ISupportInitialize)(this.pbPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbQRCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listEvent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.PictureBox pbPhoto;
        public System.Windows.Forms.Label lblfullname;
        public System.Windows.Forms.Label lblcontingent_name;
        public System.Windows.Forms.Label lblKodeParticipant;
        public System.Windows.Forms.PictureBox pbQRCode;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnUpdatPrint;
        private System.Windows.Forms.Button btnResetStatus;
        public System.Windows.Forms.Label lblKodeAkses;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblJenisPeserta;
        public System.Windows.Forms.Label lblNIK;
        private System.Windows.Forms.Button btnTutup;
        public System.Windows.Forms.Label lblParticipantID;
        public System.Windows.Forms.DataGridView listEvent;
        public System.Windows.Forms.Label lblevent_code;
        public System.Windows.Forms.Label lblevent_name;
    }
}