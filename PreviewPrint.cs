﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;


namespace PrintingIDCard
{
    public partial class PreviewPrint : Form
    {
        public PreviewPrint()
        {
            InitializeComponent();
        }

        public DataSet dsReportLive;
        public string Live = "";
        public string KodeAkses = "";
        public Boolean OfcKontingen = false;

        private void PreviewPrint_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable("Atlit");
            dt.Clear();
            dt.Columns.Add("NamaLengkap");
            dt.Columns.Add("KodeAkses");
            dt.Columns.Add("Kontingen");
            dt.Columns.Add("Cabor");
            dt.Columns.Add("FotoUrl");
            dt.Columns.Add("QRCodeUrl");
            dt.Columns.Add("KodeParticipant");
            dt.Columns.Add("JenisPeserta");
            DataRow _ravi = dt.NewRow();
            _ravi["NamaLengkap"] = "Syahri Ramadhan";
            _ravi["KodeAkses"] = "F";
            _ravi["Kontingen"] = "Jawa Barat";
            _ravi["Cabor"] = "Aerosport - Terjun Payung";
            _ravi["FotoUrl"] = "https://apigames.ponxx2020papua.com/repo/participant/participant-blob-1609996930740.jpg";
            _ravi["QRCodeUrl"] = "https://apigames.ponxx2020papua.com/qr/20210410010121";
            _ravi["KodeParticipant"] = "20210410010121";
            _ravi["JenisPeserta"] = "Atlit";
            dt.Rows.Add(_ravi);
            var dsReport = new DataSet();
            dsReport.Tables.Add(dt);
            AddImageColumn(dsReport.Tables["Atlit"], "Foto");
            AddImageColumn(dsReport.Tables["Atlit"], "QRCode");
            LoadImage(dsReport.Tables[0].Rows[0], "Foto", dsReport.Tables[0].Rows[0]["FotoUrl"].ToString());
            LoadImage(dsReport.Tables[0].Rows[0], "QRCode", dsReport.Tables[0].Rows[0]["QRCodeUrl"].ToString());
            //LoadImage(dsReport.Tables[0].Rows[0], "QRCode", dsReport.Tables[0].Rows[0]["QRCode"].ToString());
            if(Live== "")
            {
                ReportDocument rd = new ReportDocument();
                string Lokasi = Directory.GetCurrentDirectory() + "\\templatePON2.rpt";
                rd.Load(Lokasi);
                rd.SetDataSource(dsReport);
                crystalReportViewer1.ReportSource = rd;
            }
            else
            {
                
                if(KodeAkses == "" || KodeAkses == "F" || KodeAkses == "Fo" || KodeAkses == "Fox")
                {
                    if (OfcKontingen)
                    {
                        ReportDocument rd = new ReportDocument();
                        string Lokasi = "";
                        Lokasi = Directory.GetCurrentDirectory() + "\\templateOfficialKontingen.rpt";
                        //Lokasi = Directory.GetCurrentDirectory() + "\\templateDynamic2.rpt";
                        rd.Load(Lokasi);
                        rd.SetDataSource(dsReportLive);
                        //BoxObject BoxCirculation = rd.ReportDefinition.ReportObjects["BoxCirculation"] as BoxObject;
                        //TextObject txtKodeAkses = rd.ReportDefinition.ReportObjects["txtAkses"] as TextObject;
                        //BoxCirculation.FillColor = System.Drawing.Color.Blue;
                        //txtKodeAkses.Text = KodeAkses;
                        //txtKodeAkses.Color = System.Drawing.Color.Blue;
                        crystalReportViewer1.ReportSource = rd;
                    }
                    else
                    {
                        ReportDocument rd = new ReportDocument();
                        string Lokasi = "";
                        Lokasi = Directory.GetCurrentDirectory() + "\\templatePON2.rpt";
                        rd.Load(Lokasi);
                        rd.SetDataSource(dsReportLive);
                        crystalReportViewer1.ReportSource = rd;
                        
                        //Lokasi = Directory.GetCurrentDirectory() + "\\templateDynamic2.rpt";
                        //rd.Load(Lokasi);
                        //rd.SetDataSource(dsReportLive);
                        //BoxObject BoxCirculation = rd.ReportDefinition.ReportObjects["BoxCirculation"] as BoxObject;
                        //TextObject txtKodeAkses = rd.ReportDefinition.ReportObjects["txtAkses"] as TextObject;
                        //BoxCirculation.FillColor = System.Drawing.Color.Red;
                        //txtKodeAkses.Text = KodeAkses;
                        //txtKodeAkses.Color = System.Drawing.Color.Red;
                        //crystalReportViewer1.ReportSource = rd;
                    }
                }
                

                if(KodeAkses == "Fxx")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateFxxCabor.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }

                if (KodeAkses == "FxxKontingen")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateFxxKontingen.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }

                if (KodeAkses == "VIP")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateVIPB.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }

                if (KodeAkses == "A")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateA.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "A2")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateA2.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "B")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateB.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "B2")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateB2.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "B3")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateB3.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "B4")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateB4.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "C1")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateC1.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "C2")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateC2.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "D")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateD1.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "D2")
                {
                    ReportDocument rd = new ReportDocument();
                    string Lokasi = "";
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateD2.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }

            }
            
        }

        private void LoadImage(DataRow objDataRow, string strImageField, string Foto)
        {
            try
            {
                System.Net.WebClient web = new System.Net.WebClient();
                byte[] Image = null;
                  Image = web.DownloadData(Foto);
                objDataRow[strImageField] = Image;
            }
            catch
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void AddImageColumn(DataTable objDataTable, string strFieldName)
        {
            try
            {
                DataColumn objDataColumn = new DataColumn(strFieldName, Type.GetType("System.Byte[]"));
                objDataTable.Columns.Add(objDataColumn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
