﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Net;

namespace PrintingIDCard
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(PublicData.url+"/users/authenticate");
                request.Method = "POST";
                //request.Headers.Add("x-access-token", Token());
                request.ContentType = "application/x-www-form-urlencoded";
                using (var streamWriter = new System.IO.StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write("email="+txtUsername.Text+"&password="+txtPassword.Text, System.Text.Encoding.UTF8);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var response = (HttpWebResponse)request.GetResponse();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
                {
                    JObject Data = JObject.Parse(reader.ReadToEnd());
                    string token = "";
                    try
                    {
                        string useraccess = ((JValue)Data["data"]["user"]["user_access"]).ToString();
                        if(useraccess == "admin" || useraccess == "Operator Akreditasi")
                        {
                            token = ((JValue)Data["data"]["token"]).ToString();
                            //MessageBox.Show(token);
                            PublicData.token = token;
                            txtUsername.Text = "";
                            txtPassword.Text = "";
                            MainForm main = new MainForm();
                            main.ShowDialog();
                        }
                        else
                        {
                            MessageBox.Show("Maaf user anda tidak memiliki akses untuk aplikasi ini");
                        }
                       
                    }
                    catch(Exception ex) {
                        MessageBox.Show("Username dan password tidak tersedia", ex.Message);
                    }
                   
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Silahkan periksa koneksi internet kamu", ex.Message);
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormConnection frm = new FormConnection();
            frm.ShowDialog();
        }
    }
}
