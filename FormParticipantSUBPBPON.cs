﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Net;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace PrintingIDCard
{
    public partial class FormParticipantSUBPBPON : Form
    {
        public FormParticipantSUBPBPON()
        {
            InitializeComponent();
        }

        public String DataHeaderCabor(string Alamat, string IdKontingen)
        {
            string Data = "";
            var request = (HttpWebRequest)WebRequest.Create(Alamat);
            request.Method = "POST";
            request.Headers.Add("x-access-token", PublicData.token);
            request.ContentType = "application/x-www-form-urlencoded";
            using (var streamWriter = new System.IO.StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write("contingent_id=" + IdKontingen , System.Text.Encoding.UTF8);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var response = (HttpWebResponse)request.GetResponse();
            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
            {
                Data = reader.ReadToEnd();
            }
            return Data;
        }

        
        public String DataHeader(string Address)
        {
            string Data = "";
            var request = (HttpWebRequest)WebRequest.Create(Address);
            request.Method = "GET";
            request.Headers.Add("x-access-token", PublicData.token);
            var response = (HttpWebResponse)request.GetResponse();
            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
            {
                Data = reader.ReadToEnd();
            }
            return Data;
        }

        string[] codeDepartment = new string[70];
        private void Bind_ComboBoxDepartment(DataTable Rows, ComboBox cmb)
        {
            cmb.Items.Clear();
            cmb.Items.Add("--PILIH--");
            codeDepartment[0] = "0";
            int _i = 1;
            foreach (DataRow Row in Rows.Select())
            {
                cmb.Items.Add(Row[1].ToString());
                codeDepartment[_i] = Row[0].ToString();
                _i++;
            }
        }
        private void FormParticipant_Load(object sender, EventArgs e)
        {
            Bind_ComboBoxDepartment(dataDepartment("0"), cmbBidang);
            cmbBidang.SelectedIndex = 0;
            cmbCluster.SelectedIndex = 0;

        }

        private DataSet dataParticipant()
        {
            string url = "https://api.ponxx2021papua.com/workforce/registrationservice/panpels?bidang="+cmbBidang.SelectedItem.ToString()+"&department=" + cmbSubBidang.SelectedItem.ToString()+"&cluster="+cmbCluster.SelectedItem.ToString()+"&category=SUB PB PON";
            JArray o = JArray.Parse(DataHeader(url));
            DataTable dt = new DataTable("Atlit");
            dt.Columns.Add("_id");
            dt.Columns.Add("photo");
            dt.Columns.Add("Register_Number");
            dt.Columns.Add("NamaLengkap");
            dt.Columns.Add("JenisPeserta");;
            dt.Columns.Add("Kontingen");
            dt.Columns.Add("Cabor");
            dt.Columns.Add("printing_status");
            dt.Columns.Add("KodeAkses");           
            dt.Columns.Add("KodeParticipant");
            dt.Columns.Add("NIK");

            for (int i = 0; i < o.Count; i++)
            {
                string _id="", NamaLengkap="", JenisPeserta="",
                    Kontingen="", Cabor="", photo = "", Register_Number = "",
                    printing_status = "Belum Tercetak", KodeAkses = "F", KodeParticipant="", NIK = "";
                try
                {
                    _id = ((JValue)o[i]["_id"]).ToString();
                }
                catch { }
                try
                { 
                    NamaLengkap = ((JValue)o[i]["full_name"]).ToString(); }
                catch { }
                try
                {
                    NIK = ((JValue)o[i]["KTP"]).ToString();
                }
                catch { }
                try { JenisPeserta = ((JValue)o[i]["position"]).ToString();
                } catch { }
                try
                {
                    Kontingen = cmbSubBidang.SelectedItem.ToString() + " "+cmbCluster.SelectedItem.ToString();
                }
                catch { }
                try
                {
                    if ((JValue)o[i]["photo"] != null)
                    {
                        photo = ((JValue)o[i]["photo"]).ToString();
                    }
                }
                catch { }

                try
                {
                    if ((JValue)o[i]["Register_Number"] != null)
                    {
                        Register_Number = ((JValue)o[i]["Register_Number"]).ToString();
                        KodeParticipant = ((JValue)o[i]["Register_Number"]).ToString();
                    }
                }
                catch { }
                try
                {
                    if ((JValue)o[i]["printing_status"] != null)
                    {
                        if (Convert.ToBoolean((JValue)o[i]["printing_status"]) == true)
                        {
                            printing_status = "Tercetak";
                        }
                        else
                        {
                            printing_status = "Belum Tercetak";
                        }
                    }

                }
                catch { }
                dt.Rows.Add(
                    _id,
                    photo,
                    Register_Number,
                    NamaLengkap,                    
                    JenisPeserta,                    
                    Kontingen,
                    Cabor,                    
                    printing_status,
                    KodeAkses,
                    KodeParticipant,
                    NIK
               );

            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return ds;
        }

        private DataTable dataDepartment(string ParentName)
        {
            JArray o = JArray.Parse(DataHeader("https://api.ponxx2021papua.com/workforce/registrationservice/departments?category=SUB PB PON&parentName="+ ParentName));
            DataTable dt = new DataTable();
            dt.Columns.Add("code");
            dt.Columns.Add("name");
            for (int i = 0; i < o.Count; i++)
            {
                string code = "", name = "";
                try
                { code = ((JValue)o[i]["code"]).ToString(); }
                catch { }
                try
                { name = ((JValue)o[i]["bidang"]).ToString(); }
                catch { }
                dt.Rows.Add(code, name);
            }
            return dt;
        }

        private DataTable registermultidiscipline(string _ID_Number, string _Participant_Type)
        {
            DataTable dt = new DataTable("MultiDisipline");
            try
            {
                var request = (HttpWebRequest)WebRequest.Create("https://apigames.ponxx2020papua.com/participants/multidiscipline");
                request.Method = "POST";
                request.Headers.Add("x-access-token", PublicData.token);
                request.ContentType = "application/x-www-form-urlencoded";
                using (var streamWriter = new System.IO.StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write("ID_Number="+ _ID_Number + "&participant_type="+ _Participant_Type, System.Text.Encoding.UTF8);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var response = (HttpWebResponse)request.GetResponse();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
                {
                    JArray Data = JArray.Parse(reader.ReadToEnd());
                    
                    try
                    {                        
                        dt.Columns.Add("_id");
                        dt.Columns.Add("Register_Number");
                        dt.Columns.Add("full_name");
                        dt.Columns.Add("contingent_name"); ;
                        dt.Columns.Add("event_name");
                        dt.Columns.Add("event_code");

                        for (int i = 0; i < Data.Count; i++)
                        {
                            string _id = "", Register_Number = "", full_name = "",
                                contingent_name = "", event_name = "", event_code="";
                            try{ _id = ((JValue)Data[i]["_id"]).ToString();}catch { }
                            try { Register_Number = ((JValue)Data[i]["Register_Number"]).ToString(); } catch { }
                            try { full_name = ((JValue)Data[i]["full_name"]).ToString(); } catch { }
                            try { contingent_name = ((JValue)Data[i]["contingent_name"]).ToString(); } catch { }
                            try { event_name = ((JValue)Data[i]["event_name"]).ToString(); } catch { }
                            try { event_code = ((JValue)Data[i]["event_code"]).ToString(); } catch { }
                            dt.Rows.Add(
                                _id,
                                Register_Number,
                                full_name,
                                contingent_name,
                                event_name,
                                event_code
                           );

                        }
                        
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Username dan password tidak tersedia", ex.Message);
                    }

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Silahkan periksa koneksi internet kamu", ex.Message);
            }
            return dt;
        }


        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void LoadImage(DataRow objDataRow, string strImageField, string Foto)
        {
            try
            {
                System.Net.WebClient web = new System.Net.WebClient();
                byte[] Image = null;
                Image = web.DownloadData(Foto);
                objDataRow[strImageField] = Image;
            }
            catch
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void AddImageColumn(DataTable objDataTable, string strFieldName)
        {
            try
            {
                DataColumn objDataColumn = new DataColumn(strFieldName, Type.GetType("System.Byte[]"));
                objDataTable.Columns.Add(objDataColumn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public DataSet ds = new DataSet();

        private void cmb_kontingen_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbjabatan_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if(cmbdepartment.SelectedIndex == 0)
            //{
            //    btnCetakVIP.Visible = true;
            //}
            //else
            //{
            //    btnCetakVIP.Visible = false;
            //}
        }

        void PrintIDCard(string Kode)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                FormPreviewPrintSUBPBPON frm = new FormPreviewPrintSUBPBPON();
                frm.lblParticipantID.Text = dataGridView1.Rows[row.Index].Cells["_id"].Value.ToString().ToUpper();
                frm.lblfullname.Text = dataGridView1.Rows[row.Index].Cells["NamaLengkap"].Value.ToString().ToUpper();
                frm.lblcontingent_name.Text = dataGridView1.Rows[row.Index].Cells["Kontingen"].Value.ToString().ToUpper();

                frm.lblevent_name.Text = cmbSubBidang.SelectedItem.ToString() + " - " + cmbCluster.SelectedItem.ToString();
                frm.lblevent_code.Text = codeDepartment[cmbSubBidang.SelectedIndex];
                frm.photo = dataGridView1.Rows[row.Index].Cells["photo"].Value.ToString();
                frm.QRCode = dataGridView1.Rows[row.Index].Cells["Register_Number"].Value.ToString();

                frm.lblKodeParticipant.Text = dataGridView1.Rows[row.Index].Cells["Register_Number"].Value.ToString();
                frm.lblJenisPeserta.Text = dataGridView1.Rows[row.Index].Cells["JenisPeserta"].Value.ToString().ToUpper();
                frm.lblNIK.Text = dataGridView1.Rows[row.Index].Cells["NIK"].Value.ToString();

                frm.lblKodeAkses.Text = Kode;
                //frm.Ajudan = ckAjudan.Checked;
                var requestPhoto = WebRequest.Create("https://apigames.ponxx2020papua.com/repo/panpeupp/" + dataGridView1.Rows[row.Index].Cells["photo"].Value.ToString());
                using (var response = requestPhoto.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    frm.pbPhoto.Image = Bitmap.FromStream(stream);
                }
                var requestQRCode = WebRequest.Create(PublicData.url + "/qr/" + dataGridView1.Rows[row.Index].Cells["Register_Number"].Value.ToString());
                using (var response = requestQRCode.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    frm.pbQRCode.Image = Bitmap.FromStream(stream);
                }
                frm.ShowDialog();
            }
        }

        private void btnCetakVIP_Click(object sender, EventArgs e)
        {
            PrintIDCard("SubPONB2");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ds = dataParticipant();

                AddImageColumn(ds.Tables["Atlit"], "Foto");
                AddImageColumn(ds.Tables["Atlit"], "QRCode");
                //for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                //{
                //    LoadImage(ds.Tables[0].Rows[index], "Foto", "https://workforcevolume.oss-ap-southeast-5.aliyuncs.com" + ds.Tables[0].Rows[index]["photo"].ToString());
                //    LoadImage(ds.Tables[0].Rows[index], "QRCode", PublicData.url + "/qr/" + ds.Tables[0].Rows[index]["Register_Number"].ToString());
                //}
                
                dataGridView1.DataSource = ds.Tables[0];//q_data.dtIDCardAtlit(IdKejuaraan, IdKontingen_list,Provinsi,  "").Tables[0];

                int _i = 0;
                dataGridView1.Columns["_id"].Visible = false;
                dataGridView1.Columns["photo"].Visible = true;
                dataGridView1.Columns["Register_Number"].Visible = true;
                dataGridView1.Columns["KodeAkses"].Visible = false;
                dataGridView1.Columns["KodeParticipant"].Visible = false;
                dataGridView1.Columns["NIK"].Visible = false;
                dataGridView1.Columns["Foto"].Visible = false;
                dataGridView1.Columns["QRCode"].Visible = false;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells["printing_status"].Value.ToString() == "Tercetak")
                    {
                        dataGridView1.Rows[_i].DefaultCellStyle.BackColor = Color.Green;
                        dataGridView1.Rows[_i].DefaultCellStyle.ForeColor = Color.White;
                    }
                    _i++;
                }
                MessageBox.Show("Data Bind Selesai");
                btnCetakVIP.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCetakA_Click(object sender, EventArgs e)
        {
            PrintIDCard("SubPONB3");
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            PrintIDCard("SubPONB4");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            PrintIDCard("SubPONB1");
        }

        private void cmbBidang_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_ComboBoxDepartment(dataDepartment(cmbBidang.SelectedItem.ToString()), cmbSubBidang);
            cmbSubBidang.SelectedIndex = 0;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create("https://api.ponxx2021papua.com/workforce/registrationservice/public/panpels/QRCode");
                request.Method = "POST";
                //request.Headers.Add("x-access-token", Token());
                request.ContentType = "application/x-www-form-urlencoded";
                using (var streamWriter = new System.IO.StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write("department=" + cmbSubBidang.SelectedItem.ToString() + "&bidang=" + cmbBidang.SelectedItem.ToString() + "&cluster=" + cmbCluster.SelectedItem.ToString() + "&category=SUB PB PON", System.Text.Encoding.UTF8);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var response = (HttpWebResponse)request.GetResponse();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
                {
                    string resData = reader.ReadToEnd();
                    try
                    {
                        if (resData == "Finish")
                        {  
                            MessageBox.Show("QR Code Berhasil di generate");
                            button1_Click(null, null);
                        }
                        else
                        {
                            MessageBox.Show(resData);
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Username dan password tidak tersedia", ex.Message);
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Silahkan periksa koneksi internet kamu", ex.Message);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            PrintIDCard("SubPONB5");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            PrintIDCard("VIPPB1");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            PrintIDCard("VIPPB2");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            PrintIDCard("VIPPB3");
        }
    }
}
