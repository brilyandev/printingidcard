﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;


namespace PrintingIDCard
{
    public partial class PreviewPrintCdM : Form
    {
        public PreviewPrintCdM()
        {
            InitializeComponent();
        }

        public DataSet dsReportLive;
        public string Live = "";
        public string KodeAkses = "";

        private void PreviewPrint_Load(object sender, EventArgs e)
        {
            
            //DataTable dt = new DataTable("Atlit");
            //dt.Clear();
            //dt.Columns.Add("NamaLengkap");
            //dt.Columns.Add("KodeAkses");
            //dt.Columns.Add("Kontingen");
            //dt.Columns.Add("Cabor");
            //dt.Columns.Add("FotoUrl");
            //dt.Columns.Add("QRCodeUrl");
            //dt.Columns.Add("KodeParticipant");
            //dt.Columns.Add("JenisPeserta");
            //DataRow _ravi = dt.NewRow();
            //_ravi["NamaLengkap"] = "Syahri Ramadhan";
            //_ravi["KodeAkses"] = "F";
            //_ravi["Kontingen"] = "Jawa Barat";
            //_ravi["Cabor"] = "Aerosport - Terjun Payung";
            //_ravi["FotoUrl"] = "https://apimeeting.ponxx2020papua.com/repo/participant/participant-blob-1609996930740.jpg";
            //_ravi["QRCodeUrl"] = "https://apigames.ponxx2020papua.com/qr/20210410010121";
            //_ravi["KodeParticipant"] = "20210410010121";
            //_ravi["JenisPeserta"] = "Atlit";
            //dt.Rows.Add(_ravi);
            //var dsReport = new DataSet();
            //dsReport.Tables.Add(dt);
            //AddImageColumn(dsReport.Tables["Atlit"], "Foto");
            //AddImageColumn(dsReport.Tables["Atlit"], "QRCode");
            //LoadImage(dsReport.Tables[0].Rows[0], "Foto", dsReport.Tables[0].Rows[0]["FotoUrl"].ToString());
            //LoadImage(dsReport.Tables[0].Rows[0], "QRCode", dsReport.Tables[0].Rows[0]["QRCodeUrl"].ToString());
            //LoadImage(dsReport.Tables[0].Rows[0], "QRCode", dsReport.Tables[0].Rows[0]["QRCode"].ToString());
            if(Live== "")
            {
                ReportDocument rd = new ReportDocument();
                string Lokasi = Directory.GetCurrentDirectory() + "\\templateCdM.rpt";
                rd.Load(Lokasi);
                rd.SetDataSource(dsReportLive);
                crystalReportViewer1.ReportSource = rd;
            }
            else
            {
                ReportDocument rd = new ReportDocument();
                string Lokasi = "";
                if (KodeAkses == "Temporary")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateTemporary1.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }

                if (KodeAkses == "VIP")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateVIP.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "VIP2")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateVIPB.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }

                if (KodeAkses == "A")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateA.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "A2")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateA2.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "B")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateB.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "B2")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateB2.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "B3")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateB3.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "B4")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateB4.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "C1")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateC1.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "C2")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateC2.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "D")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateD1.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "D2")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateD2.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }

                if (KodeAkses == "SubPONB1")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateSUBPON01.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "SubPONB2")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateSUBPON02.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "SubPONB3")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateSUBPON03.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "SubPONB4")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateSUBPON04.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "SubPONB5")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateSUBPON05.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }

                if (KodeAkses == "VIPPB1")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateKoordinator1.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "VIPPB2")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateKoordinator2.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "VIPPB3")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateKoordinator3.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }


                if (KodeAkses == "SDM1")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateVolunteer01.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "SDM2")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateVolunteer02.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "SDM3")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateVolunteer03.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }


                if (KodeAkses == "PERS1")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateWartawan.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "PERS2")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateWartawan2.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }

                if (KodeAkses == "G")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateSatgas.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }

                if (KodeAkses == "OCC1")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateOCC.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "OCC2")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateOCC01.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
                if (KodeAkses == "OCC3")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateOCC02.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }

                if (KodeAkses == "BROADCAST1")
                {
                    Lokasi = Directory.GetCurrentDirectory() + "\\templateBroadcast1.rpt";
                    rd.Load(Lokasi);
                    rd.SetDataSource(dsReportLive);
                    crystalReportViewer1.ReportSource = rd;
                }
            }
            
        }

        private void LoadImage(DataRow objDataRow, string strImageField, string Foto)
        {
            try
            {
                System.Net.WebClient web = new System.Net.WebClient();
                byte[] Image = null;
                  Image = web.DownloadData(Foto);
                objDataRow[strImageField] = Image;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddImageColumn(DataTable objDataTable, string strFieldName)
        {
            try
            {
                DataColumn objDataColumn = new DataColumn(strFieldName, Type.GetType("System.Byte[]"));
                objDataTable.Columns.Add(objDataColumn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
