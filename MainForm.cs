﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using Quobject.SocketIoClientDotNet.Client;
using System.Drawing.Printing;
using System.Printing;

namespace PrintingIDCard
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string hari = "";
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(DateTime.Now);
            if (day == DayOfWeek.Monday)
            {
                hari = "Senin";
            }
            if (day == DayOfWeek.Sunday)
            {
                hari = "Minggu";
            }
            if (day == DayOfWeek.Tuesday)
            {
                hari = "Selasa";
            }
            if (day == DayOfWeek.Wednesday)
            {
                hari = "Rabu";
            }
            if (day == DayOfWeek.Thursday)
            {
                hari = "Kamis";
            }
            if (day == DayOfWeek.Friday)
            {
                hari = "Jumat";
            }
            if (day == DayOfWeek.Saturday)
            {
                hari = "Sabtu";
            }
            string bulan = "";
            if (DateTime.Now.Month == 1)
            {
                bulan = "Januari";
            }
            if (DateTime.Now.Month == 2)
            {
                bulan = "Februari";
            }
            if (DateTime.Now.Month == 3)
            {
                bulan = "Maret";
            }
            if (DateTime.Now.Month == 4)
            {
                bulan = "April";
            }
            if (DateTime.Now.Month == 5)
            {
                bulan = "Mei";
            }
            if (DateTime.Now.Month == 6)
            {
                bulan = "Juni";
            }
            if (DateTime.Now.Month == 7)
            {
                bulan = "Juli";
            }
            if (DateTime.Now.Month == 8)
            {
                bulan = "Agustus";
            }
            if (DateTime.Now.Month == 9)
            {
                bulan = "September";
            }
            if (DateTime.Now.Month == 10)
            {
                bulan = "Oktober";
            }
            if (DateTime.Now.Month == 11)
            {
                bulan = "November";
            }
            if (DateTime.Now.Month == 12)
            {
                bulan = "Desember";
            }
            lbDate.Text = hari + " , " + DateTime.Now.Day.ToString() + " " + bulan + " " + DateTime.Now.Year.ToString();
            lbTimer.Text = DateTime.Now.ToString("HH:mm:ss");
        }

        private void btnTestPrint_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable("Atlit");
            dt.Clear();
            dt.Columns.Add("NamaLengkap");
            dt.Columns.Add("KodeAkses");
            dt.Columns.Add("Kontingen");
            dt.Columns.Add("Cabor");
            dt.Columns.Add("Cabor2");
            dt.Columns.Add("Cabor3");
            dt.Columns.Add("KodeCabor");
            dt.Columns.Add("KodeCabor2");
            dt.Columns.Add("KodeCabor3");
            dt.Columns.Add("FotoUrl");
            dt.Columns.Add("QRCodeUrl");
            dt.Columns.Add("KodeParticipant");
            
            DataRow _ravi = dt.NewRow();
            _ravi["NamaLengkap"] = "Tempory Name";
            _ravi["KodeAkses"] = "T";
            _ravi["Kontingen"] = "Jawa Barat";
            _ravi["Cabor"] = "Temporary";
            _ravi["KodeCabor"] = "TID";
            _ravi["FotoUrl"] = PublicData.url + "/repo/nophoto.png";
            _ravi["QRCodeUrl"] = PublicData.url + "/qr/1234567890";
            _ravi["KodeParticipant"] = "1234567890";
            dt.Rows.Add(_ravi);
            var dsReport = new DataSet();
            dsReport.Tables.Add(dt);
            AddImageColumn(dsReport.Tables["Atlit"], "Foto");
            AddImageColumn(dsReport.Tables["Atlit"], "QRCode");
            LoadImage(dsReport.Tables[0].Rows[0], "Foto", dsReport.Tables[0].Rows[0]["FotoUrl"].ToString());
            LoadImage(dsReport.Tables[0].Rows[0], "QRCode", dsReport.Tables[0].Rows[0]["QRCodeUrl"].ToString());
            PreviewPrintCdM frm = new PreviewPrintCdM();
            frm.Live = "Live";
            frm.dsReportLive = dsReport;
            frm.KodeAkses = "Temporary";
            frm.ShowDialog();
            //ReportDocument rd = new ReportDocument();
            //string Lokasi = Directory.GetCurrentDirectory() + "\\templateTemporary.rpt";
            //rd.Load(Lokasi);
            //rd.SetDataSource(dsReport);
            //rd.PrintToPrinter(1, true, 0, 0);
        }

        public void LoadImage(DataRow objDataRow, string strImageField, string Foto)
        {
            try
            {
                System.Net.WebClient web = new System.Net.WebClient();
                byte[] Image = null;
                Image = web.DownloadData(Foto);
                objDataRow[strImageField] = Image;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void AddImageColumn(DataTable objDataTable, string strFieldName)
        {
            try
            {
                DataColumn objDataColumn = new DataColumn(strFieldName, Type.GetType("System.Byte[]"));
                objDataTable.Columns.Add(objDataColumn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            PreviewPrint frm = new PreviewPrint();
            frm.ShowDialog();
            frm.Close();
        }

        delegate void SetTextCallback(string text);

        //private void SetTextStatus(string text)
        //{
        //    // InvokeRequired required compares the thread ID of the
        //    // calling thread to the thread ID of the creating thread.
        //    //If these threads are different, it returns true.
        //    if (lblStatus.InvokeRequired)
        //    {
        //        SetTextCallback d = new SetTextCallback(SetTextStatus);
        //        this.Invoke(d, new object[] { text });
        //    }
        //    else
        //    {
        //        lblStatus.Text = text;
        //    }
        //}

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                lblToken.Text = PublicData.token;
                linkConnection.Text = "Server : " + PublicData.url;
                //var socketIO = IO.Socket("http://localhost:3535");
                ////var socketIO = IO.Socket("http://202.83.121.244:4000");
                ////Console.WriteLine(socketIO);
                //socketIO.On(Socket.EVENT_CONNECT, () =>
                //{
                //    SetTextStatus("Connected");
                //});

                //socketIO.On("print", (data) =>
                //{
                //    SetTextJobStatus((string)data);
                //    btnTestPrint_Click(null, null);

                //});
                //socketIO.On("chat message", (data) =>
                //{
                //    MessageBox.Show((string)data);
                //});

                //foreach (string printerName in PrinterSettings.InstalledPrinters)
                //{
                //    lblPrinterStatus.Text = "Printer : "+printerName;
                //    Console.WriteLine("Printer: {0}", printerName);
                //    listBox1.Items.Add(printerName);
                //}
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //private void listBox1_DoubleClick(object sender, EventArgs e)
        //{
        //    lblPrinterStatus.Text = listBox1.SelectedItem.ToString();
        //    queue = server.GetPrintQueue(listBox1.SelectedItem.ToString(), new string[0] { });
        //    Console.WriteLine(queue);
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            FormParticipant frm = new FormParticipant();
            frm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PreviewPrint frm = new PreviewPrint();
            frm.ShowDialog();
            frm.Close();
        }

        private void MainForm_Leave(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormParticipantOfficialCabor frm = new FormParticipantOfficialCabor();
            frm.ShowDialog();
        }

        private void linkConnection_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FormConnection frm = new FormConnection();
            frm.ShowDialog();
            linkConnection.Text = PublicData.url;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FormParticipantVIPKontingen frm = new FormParticipantVIPKontingen();
            frm.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            FormParticipantLembagaNegara frm = new FormParticipantLembagaNegara();
            frm.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FormParticipantPanwasrah frm = new FormParticipantPanwasrah();
            frm.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            FormParticipantKementerian frm = new FormParticipantKementerian();
            frm.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            FormParticipantOfficialKontingen frm = new FormParticipantOfficialKontingen();
            frm.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            FormParticipantPanpelCabor frm = new FormParticipantPanpelCabor();
            frm.ShowDialog();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            FormParticipantSUBPBPON frm = new FormParticipantSUBPBPON();
            frm.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            FormParticipantPBPON frm = new FormParticipantPBPON();
            frm.ShowDialog();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            FormParticipantWartawan frm = new FormParticipantWartawan();
            frm.ShowDialog();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            FormParticipantSDM frm = new FormParticipantSDM();
            frm.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            FormParticipantSDM frm = new FormParticipantSDM();
            frm.ShowDialog();
        }

        private void button15_Click_1(object sender, EventArgs e)
        {
            FormParticipantSatgas frm = new FormParticipantSatgas();
            frm.ShowDialog();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            FormParticipantOCC frm = new FormParticipantOCC();
            frm.ShowDialog();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            FormParticipantBroadcast frm = new FormParticipantBroadcast();
            frm.ShowDialog();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            FormParticipantVIP frm = new FormParticipantVIP();
            frm.ShowDialog();
        }
    }
}
