﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrintingIDCard
{
    public partial class FormConnection : Form
    {
        public FormConnection()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PublicData.url = textBox1.Text;
            MessageBox.Show("Koneksi ke server telah diatur");
        }

        private void FormConnection_Load(object sender, EventArgs e)
        {
            textBox1.Text = PublicData.url;
        }
    }
}
