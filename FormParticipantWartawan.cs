﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Net;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace PrintingIDCard
{
    public partial class FormParticipantWartawan : Form
    {
        public FormParticipantWartawan()
        {
            InitializeComponent();
        }

        public String DataHeaderCabor(string Alamat, string IdKontingen)
        {
            string Data = "";
            var request = (HttpWebRequest)WebRequest.Create(Alamat);
            request.Method = "POST";
            request.Headers.Add("x-access-token", PublicData.token);
            request.ContentType = "application/x-www-form-urlencoded";
            using (var streamWriter = new System.IO.StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write("contingent_id=" + IdKontingen , System.Text.Encoding.UTF8);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var response = (HttpWebResponse)request.GetResponse();
            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
            {
                Data = reader.ReadToEnd();
            }
            return Data;
        }

        
        public String DataHeader(string Address)
        {
            string Data = "";
            var request = (HttpWebRequest)WebRequest.Create(Address);
            request.Method = "GET";
            request.Headers.Add("x-access-token", PublicData.token);
            var response = (HttpWebResponse)request.GetResponse();
            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
            {
                Data = reader.ReadToEnd();
            }
            return Data;
        }

        string[] codeDepartment = new string[70];
        private void Bind_ComboBoxDepartment(DataTable Rows, ComboBox cmb)
        {
            cmb.Items.Clear();
            cmb.Items.Add("--PILIH--");
            codeDepartment[0] = "0";
            int _i = 1;
            foreach (DataRow Row in Rows.Select())
            {
                cmb.Items.Add(Row[1].ToString());
                codeDepartment[_i] = Row[0].ToString();
                _i++;
            }
        }
        private void FormParticipant_Load(object sender, EventArgs e)
        {
            Bind_ComboBoxDepartment(dataDepartment(), cmbdepartment);
            cmbdepartment.SelectedIndex = 0;
        }

        private DataSet dataParticipant()
        {
            string url = "https://api.ponxx2021papua.com/workforce/registrationservice/private/registration?department=" + cmbdepartment.SelectedItem.ToString();
            JArray o = JArray.Parse(DataHeader(url));
            DataTable dt = new DataTable("Atlit");
            dt.Columns.Add("_id");
            dt.Columns.Add("photo");
            dt.Columns.Add("Register_Number");
            dt.Columns.Add("NamaLengkap");
            dt.Columns.Add("JenisPeserta");;
            dt.Columns.Add("Kontingen");
            dt.Columns.Add("Cabor");
            dt.Columns.Add("printing_status");
            dt.Columns.Add("KodeAkses");           
            dt.Columns.Add("KodeParticipant");
            dt.Columns.Add("NIK");

            for (int i = 0; i < o.Count; i++)
            {
                string _id="", NamaLengkap="", JenisPeserta="",
                    Kontingen="", Cabor="", photo = "", Register_Number = "1234",
                    printing_status = "Belum Tercetak", KodeAkses = "F", KodeParticipant="", NIK = "";
                try
                {
                    _id = ((JValue)o[i]["_id"]).ToString();
                }
                catch { }
                try
                { 
                    NamaLengkap = ((JValue)o[i]["full_name"]).ToString(); }
                catch { }
                try
                {
                    if (((JValue)o[i]["ID_Number"]) != null)
                    {
                        NIK = ((JValue)o[i]["ID_Number"]).ToString();
                    }
                }
                catch { }
                try {
                    if (((JValue)o[i]["position"]) != null)
                    {
                        JenisPeserta = ((JValue)o[i]["position"]).ToString();
                    }

                } catch { }
                try
                {
                    Kontingen = cmbdepartment.SelectedItem.ToString();
                }
                catch { }
                try
                {
                    if(((JValue)o[i]["class_media"])!= null)
                    {
                        Cabor = ((JValue)o[i]["class_media"]).ToString();
                    }
                }
                catch { }
                try
                {
                    if ((JValue)o[i]["photo"] != null)
                    {
                        photo = ((JValue)o[i]["photo"]).ToString();
                    }
                }
                catch { }


                try
                {

                    //Wartawan Kontingen
                    //RRI Jakarta
                    //Media Online Nasional
                    //TVRI Jakarta
                    //Media Papua
                    //Media Cetak Nasional
                    //TV Nasional
                    if (cmbdepartment.SelectedItem.ToString() == "RRI Jakarta")
                    {
                        KodeAkses = "ER";
                    }
                    else if (cmbdepartment.SelectedItem.ToString() == "Media Online Nasional")
                    {
                        KodeAkses = "EOn";
                    }
                    else if(cmbdepartment.SelectedItem.ToString() == "TVRI Jakarta" || cmbdepartment.SelectedItem.ToString() == "TV Nasional")
                    {
                        KodeAkses = "ETV";
                    }
                    else if(cmbdepartment.SelectedItem.ToString() == "Media Papua")
                    {
                        KodeAkses = "E";
                    }
                    else if(cmbdepartment.SelectedItem.ToString() == "Media Cetak Nasional")
                    {
                        KodeAkses = "E";
                    }
                    else
                    {
                        KodeAkses = "E";
                    }
                   
                }
                catch { }

                try
                {
                    if ((JValue)o[i]["Register_Number"] != null)
                    {
                        Register_Number = ((JValue)o[i]["Register_Number"]).ToString();
                        KodeParticipant = ((JValue)o[i]["Register_Number"]).ToString();
                    }
                }
                catch { }
                try
                {
                    if ((JValue)o[i]["printing_status"] != null)
                    {
                        if (Convert.ToBoolean((JValue)o[i]["printing_status"]) == true)
                        {
                            printing_status = "Tercetak";
                        }
                        else
                        {
                            printing_status = "Belum Tercetak";
                        }
                    }

                }
                catch { }
                dt.Rows.Add(
                    _id,
                    photo,
                    Register_Number,
                    NamaLengkap,                    
                    JenisPeserta,                    
                    Kontingen,
                    Cabor,                    
                    printing_status,
                    KodeAkses,
                    KodeParticipant,
                    NIK
               );

            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return ds;
        }


        private DataTable dataDepartment()
        {
            JArray o = JArray.Parse(DataHeader("https://api.ponxx2021papua.com/workforce/registrationservice/departments?category=Wartawan"));
            DataTable dt = new DataTable();
            dt.Columns.Add("code");
            dt.Columns.Add("name");
            for (int i = 0; i < o.Count; i++)
            {
                string _id = "", name = "";
                try
                { _id = ((JValue)o[i]["code"]).ToString(); }
                catch { }
                try
                { name = ((JValue)o[i]["bidang"]).ToString(); }
                catch { }
                dt.Rows.Add(_id, name);
            }
            return dt;
        }

        private DataTable registermultidiscipline(string _ID_Number, string _Participant_Type)
        {
            DataTable dt = new DataTable("MultiDisipline");
            try
            {
                var request = (HttpWebRequest)WebRequest.Create("https://apigames.ponxx2020papua.com/participants/multidiscipline");
                request.Method = "POST";
                request.Headers.Add("x-access-token", PublicData.token);
                request.ContentType = "application/x-www-form-urlencoded";
                using (var streamWriter = new System.IO.StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write("ID_Number="+ _ID_Number + "&participant_type="+ _Participant_Type, System.Text.Encoding.UTF8);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var response = (HttpWebResponse)request.GetResponse();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
                {
                    JArray Data = JArray.Parse(reader.ReadToEnd());
                    
                    try
                    {                        
                        dt.Columns.Add("_id");
                        dt.Columns.Add("Register_Number");
                        dt.Columns.Add("full_name");
                        dt.Columns.Add("contingent_name"); ;
                        dt.Columns.Add("event_name");
                        dt.Columns.Add("event_code");

                        for (int i = 0; i < Data.Count; i++)
                        {
                            string _id = "", Register_Number = "", full_name = "",
                                contingent_name = "", event_name = "", event_code="";
                            try{ _id = ((JValue)Data[i]["_id"]).ToString();}catch { }
                            try { Register_Number = ((JValue)Data[i]["Register_Number"]).ToString(); } catch { }
                            try { full_name = ((JValue)Data[i]["full_name"]).ToString(); } catch { }
                            try { contingent_name = ((JValue)Data[i]["contingent_name"]).ToString(); } catch { }
                            try { event_name = ((JValue)Data[i]["event_name"]).ToString(); } catch { }
                            try { event_code = ((JValue)Data[i]["event_code"]).ToString(); } catch { }
                            dt.Rows.Add(
                                _id,
                                Register_Number,
                                full_name,
                                contingent_name,
                                event_name,
                                event_code
                           );

                        }
                        
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Username dan password tidak tersedia", ex.Message);
                    }

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Silahkan periksa koneksi internet kamu", ex.Message);
            }
            return dt;
        }


        private void button2_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                FormPreviewPrintVIPKontingen frm = new FormPreviewPrintVIPKontingen();
                frm.lblParticipantID.Text = dataGridView1.Rows[row.Index].Cells["_id"].Value.ToString().ToUpper();
                frm.lblfullname.Text = dataGridView1.Rows[row.Index].Cells["NamaLengkap"].Value.ToString().ToUpper();
                frm.lblcontingent_name.Text = dataGridView1.Rows[row.Index].Cells["Kontingen"].Value.ToString().ToUpper();

                var dataevent = "";//dataKontingenCode();
                var index = 0;
                frm.lblevent_name.Text = dataGridView1.Rows[row.Index].Cells["Kontingen"].Value.ToString().ToUpper();
                frm.lblevent_code.Text = "VIP";
                frm.photo = dataGridView1.Rows[row.Index].Cells["photo"].Value.ToString();
                frm.QRCode = dataGridView1.Rows[row.Index].Cells["Register_Number"].Value.ToString();
                //frm.lblevent_name.Text = eventlist;//dataGridView1.Rows[row.Index].Cells["Cabor"].Value.ToString();
                frm.lblKodeParticipant.Text = dataGridView1.Rows[row.Index].Cells["Register_Number"].Value.ToString();
                frm.lblJenisPeserta.Text = dataGridView1.Rows[row.Index].Cells["JenisPeserta"].Value.ToString().ToUpper()+ " "+ dataGridView1.Rows[row.Index].Cells["Cabor"].Value.ToString().ToUpper();
                frm.lblNIK.Text = dataGridView1.Rows[row.Index].Cells["NIK"].Value.ToString();

                //frm.listEvent.DataSource = registermultidiscipline(dataGridView1.Rows[row.Index].Cells["NIK"].Value.ToString(), 
                //                            dataGridView1.Rows[row.Index].Cells["JenisPeserta"].Value.ToString());

                frm.lblKodeAkses.Text = "VIP";//dataGridView1.Rows[row.Index].Cells["KodeAkses"].Value.ToString();
                var requestPhoto = WebRequest.Create(PublicData.url+"/repo/participant/" + dataGridView1.Rows[row.Index].Cells["photo"].Value.ToString());
                using (var response = requestPhoto.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    frm.pbPhoto.Image = Bitmap.FromStream(stream);
                }
                var requestQRCode = WebRequest.Create(PublicData.url + "/qr/" + dataGridView1.Rows[row.Index].Cells["Register_Number"].Value.ToString());
                using (var response = requestQRCode.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    frm.pbQRCode.Image = Bitmap.FromStream(stream);
                }
                frm.ShowDialog();
            }
        }

        private void LoadImage(DataRow objDataRow, string strImageField, string Foto)
        {
            try
            {
                System.Net.WebClient web = new System.Net.WebClient();
                byte[] Image = null;
                Image = web.DownloadData(Foto);
                objDataRow[strImageField] = Image;
            }
            catch
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void AddImageColumn(DataTable objDataTable, string strFieldName)
        {
            try
            {
                DataColumn objDataColumn = new DataColumn(strFieldName, Type.GetType("System.Byte[]"));
                objDataTable.Columns.Add(objDataColumn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public DataSet ds = new DataSet();

        private void cmb_kontingen_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbjabatan_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if(cmbdepartment.SelectedIndex == 0)
            //{
            //    btnCetakVIP.Visible = true;
            //}
            //else
            //{
            //    btnCetakVIP.Visible = false;
            //}
        }

        private void btnCetakVIP_Click(object sender, EventArgs e)
        {
            PrintIDCard("PERS3");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ds = dataParticipant();

                //AddImageColumn(ds.Tables["Atlit"], "Foto");
                //AddImageColumn(ds.Tables["Atlit"], "QRCode");
                //for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                //{
                //    LoadImage(ds.Tables[0].Rows[index], "Foto", "https://workforcevolume.oss-ap-southeast-5.aliyuncs.com" + ds.Tables[0].Rows[index]["photo"].ToString());
                //    LoadImage(ds.Tables[0].Rows[index], "QRCode", PublicData.url + "/qr/" + ds.Tables[0].Rows[index]["Register_Number"].ToString());
                //}
                
                dataGridView1.DataSource = ds.Tables[0];//q_data.dtIDCardAtlit(IdKejuaraan, IdKontingen_list,Provinsi,  "").Tables[0];

                int _i = 0;
                dataGridView1.Columns["_id"].Visible = false;
                dataGridView1.Columns["photo"].Visible = true;
                dataGridView1.Columns["Register_Number"].Visible = true;
                dataGridView1.Columns["KodeAkses"].Visible = false;
                dataGridView1.Columns["KodeParticipant"].Visible = false;
                dataGridView1.Columns["NIK"].Visible = false;
                dataGridView1.Columns["Foto"].Visible = false;
                dataGridView1.Columns["QRCode"].Visible = false;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells["printing_status"].Value.ToString() == "Tercetak")
                    {
                        dataGridView1.Rows[_i].DefaultCellStyle.BackColor = Color.Green;
                        dataGridView1.Rows[_i].DefaultCellStyle.ForeColor = Color.White;
                    }
                    _i++;
                }
                MessageBox.Show("Data Bind Selesai");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            PrintIDCard("PERS5");
        }

        void PrintIDCard(string Kode)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                FormPreviewPrintWartawan frm = new FormPreviewPrintWartawan();
                frm.lblParticipantID.Text = dataGridView1.Rows[row.Index].Cells["_id"].Value.ToString().ToUpper();
                frm.lblfullname.Text = dataGridView1.Rows[row.Index].Cells["NamaLengkap"].Value.ToString().ToUpper();
                frm.lblcontingent_name.Text = dataGridView1.Rows[row.Index].Cells["Kontingen"].Value.ToString().ToUpper() + " "+ dataGridView1.Rows[row.Index].Cells["Cabor"].Value.ToString().ToUpper();

                frm.lblevent_name.Text = cmbdepartment.SelectedItem.ToString();
                frm.lblevent_code.Text = "PERS";//codeDepartment[cmbdepartment.SelectedIndex];
                frm.photo = dataGridView1.Rows[row.Index].Cells["photo"].Value.ToString();
                frm.QRCode = dataGridView1.Rows[row.Index].Cells["Register_Number"].Value.ToString();

                frm.lblKodeParticipant.Text = dataGridView1.Rows[row.Index].Cells["Register_Number"].Value.ToString();
                frm.lblJenisPeserta.Text = dataGridView1.Rows[row.Index].Cells["JenisPeserta"].Value.ToString().ToUpper();
                frm.lblNIK.Text = dataGridView1.Rows[row.Index].Cells["NIK"].Value.ToString();

                frm.lblKodeAkses.Text = dataGridView1.Rows[row.Index].Cells["KodeAkses"].Value.ToString();
                frm.Ajudan = ckAjudan.Checked;
                frm.KodeAkses = Kode;
                var requestPhoto = WebRequest.Create("https://workforcevolume.oss-ap-southeast-5.aliyuncs.com" + dataGridView1.Rows[row.Index].Cells["photo"].Value.ToString());
                //var requestPhoto = WebRequest.Create("https://apisdm.ponxx2020papua.com/repo/participant/participant-20210401_125527.jpg-1617251160280.jpg");
                using (var response = requestPhoto.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    frm.pbPhoto.Image = Bitmap.FromStream(stream);
                }
                var requestQRCode = WebRequest.Create(PublicData.url + "/qr/" + dataGridView1.Rows[row.Index].Cells["Register_Number"].Value.ToString());
                using (var response = requestQRCode.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    frm.pbQRCode.Image = Bitmap.FromStream(stream);
                }
                frm.ShowDialog();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            PrintIDCard("PERS1");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            PrintIDCard("PERS4");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            PrintIDCard("B2");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            PrintIDCard("B3");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            PrintIDCard("PERS2");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create("https://api.ponxx2021papua.com/workforce/registrationservice/registration/QRCode");
                request.Method = "POST";
                //request.Headers.Add("x-access-token", Token());
                request.ContentType = "application/x-www-form-urlencoded";
                using (var streamWriter = new System.IO.StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write("department=" + cmbdepartment.SelectedItem.ToString(), System.Text.Encoding.UTF8);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var response = (HttpWebResponse)request.GetResponse();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
                {
                    string resData = reader.ReadToEnd();
                    try
                    {
                        if (resData == "Finish")
                        {
                            MessageBox.Show("QR Code Berhasil di generate");
                            button1_Click(null, null);
                        }
                        else
                        {
                            MessageBox.Show(resData);
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Username dan password tidak tersedia", ex.Message);
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Silahkan periksa koneksi internet kamu", ex.Message);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            PrintIDCard("B4");
        }
    }
}
