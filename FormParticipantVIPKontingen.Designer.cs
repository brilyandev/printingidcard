﻿
namespace PrintingIDCard
{
    partial class FormParticipantVIPKontingen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCetakA = new System.Windows.Forms.Button();
            this.cmb_kontingen = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Foto = new System.Windows.Forms.DataGridViewImageColumn();
            this.QRCode = new System.Windows.Forms.DataGridViewImageColumn();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnCetakB = new System.Windows.Forms.Button();
            this.btnCetakVIP = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbjabatan = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnKodeA2 = new System.Windows.Forms.Button();
            this.btnKodeB2 = new System.Windows.Forms.Button();
            this.btnKodeB3 = new System.Windows.Forms.Button();
            this.ckAjudan = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(168, 86);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 29;
            this.button1.Text = "Tampilkan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 56);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 17);
            this.label1.TabIndex = 28;
            this.label1.Text = "Pilih Kontingen";
            // 
            // btnCetakA
            // 
            this.btnCetakA.Location = new System.Drawing.Point(276, 86);
            this.btnCetakA.Margin = new System.Windows.Forms.Padding(4);
            this.btnCetakA.Name = "btnCetakA";
            this.btnCetakA.Size = new System.Drawing.Size(134, 28);
            this.btnCetakA.TabIndex = 31;
            this.btnCetakA.Text = "Cetak Kode A1";
            this.btnCetakA.UseVisualStyleBackColor = true;
            this.btnCetakA.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmb_kontingen
            // 
            this.cmb_kontingen.FormattingEnabled = true;
            this.cmb_kontingen.Location = new System.Drawing.Point(168, 53);
            this.cmb_kontingen.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_kontingen.Name = "cmb_kontingen";
            this.cmb_kontingen.Size = new System.Drawing.Size(228, 24);
            this.cmb_kontingen.TabIndex = 27;
            this.cmb_kontingen.SelectedIndexChanged += new System.EventHandler(this.cmb_kontingen_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Foto,
            this.QRCode});
            this.dataGridView1.Location = new System.Drawing.Point(13, 135);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 80;
            this.dataGridView1.Size = new System.Drawing.Size(1414, 634);
            this.dataGridView1.TabIndex = 30;
            // 
            // Foto
            // 
            this.Foto.DataPropertyName = "Foto";
            this.Foto.HeaderText = "Foto";
            this.Foto.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Foto.MinimumWidth = 6;
            this.Foto.Name = "Foto";
            this.Foto.ReadOnly = true;
            this.Foto.Width = 125;
            // 
            // QRCode
            // 
            this.QRCode.DataPropertyName = "QRCode";
            this.QRCode.HeaderText = "QRCode";
            this.QRCode.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.QRCode.MinimumWidth = 6;
            this.QRCode.Name = "QRCode";
            this.QRCode.ReadOnly = true;
            this.QRCode.Width = 125;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(762, 21);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(228, 24);
            this.comboBox1.TabIndex = 32;
            this.comboBox1.Visible = false;
            // 
            // btnCetakB
            // 
            this.btnCetakB.Location = new System.Drawing.Point(563, 86);
            this.btnCetakB.Margin = new System.Windows.Forms.Padding(4);
            this.btnCetakB.Name = "btnCetakB";
            this.btnCetakB.Size = new System.Drawing.Size(134, 28);
            this.btnCetakB.TabIndex = 33;
            this.btnCetakB.Text = "Cetak Kode B1";
            this.btnCetakB.UseVisualStyleBackColor = true;
            this.btnCetakB.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnCetakVIP
            // 
            this.btnCetakVIP.Location = new System.Drawing.Point(276, 85);
            this.btnCetakVIP.Margin = new System.Windows.Forms.Padding(4);
            this.btnCetakVIP.Name = "btnCetakVIP";
            this.btnCetakVIP.Size = new System.Drawing.Size(134, 28);
            this.btnCetakVIP.TabIndex = 34;
            this.btnCetakVIP.Text = "Cetak Kode VIP";
            this.btnCetakVIP.UseVisualStyleBackColor = true;
            this.btnCetakVIP.Click += new System.EventHandler(this.btnCetakVIP_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 24);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 17);
            this.label2.TabIndex = 36;
            this.label2.Text = "Pilih Jabatan";
            // 
            // cmbjabatan
            // 
            this.cmbjabatan.FormattingEnabled = true;
            this.cmbjabatan.Items.AddRange(new object[] {
            "VIP",
            "CdM",
            "KONI"});
            this.cmbjabatan.Location = new System.Drawing.Point(168, 21);
            this.cmbjabatan.Margin = new System.Windows.Forms.Padding(4);
            this.cmbjabatan.Name = "cmbjabatan";
            this.cmbjabatan.Size = new System.Drawing.Size(228, 24);
            this.cmbjabatan.TabIndex = 35;
            this.cmbjabatan.SelectedIndexChanged += new System.EventHandler(this.cmbjabatan_SelectedIndexChanged);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(583, 17);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(171, 28);
            this.button4.TabIndex = 42;
            this.button4.Text = "Reset QR Code";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(404, 17);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(171, 28);
            this.button3.TabIndex = 41;
            this.button3.Text = "Generate QR Code";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnKodeA2
            // 
            this.btnKodeA2.Location = new System.Drawing.Point(418, 86);
            this.btnKodeA2.Margin = new System.Windows.Forms.Padding(4);
            this.btnKodeA2.Name = "btnKodeA2";
            this.btnKodeA2.Size = new System.Drawing.Size(134, 28);
            this.btnKodeA2.TabIndex = 44;
            this.btnKodeA2.Text = "Cetak Kode A2";
            this.btnKodeA2.UseVisualStyleBackColor = true;
            this.btnKodeA2.Click += new System.EventHandler(this.btnKodeA2_Click);
            // 
            // btnKodeB2
            // 
            this.btnKodeB2.Location = new System.Drawing.Point(705, 85);
            this.btnKodeB2.Margin = new System.Windows.Forms.Padding(4);
            this.btnKodeB2.Name = "btnKodeB2";
            this.btnKodeB2.Size = new System.Drawing.Size(134, 28);
            this.btnKodeB2.TabIndex = 45;
            this.btnKodeB2.Text = "Cetak Kode B2";
            this.btnKodeB2.UseVisualStyleBackColor = true;
            this.btnKodeB2.Click += new System.EventHandler(this.btnKodeB2_Click);
            // 
            // btnKodeB3
            // 
            this.btnKodeB3.Location = new System.Drawing.Point(847, 85);
            this.btnKodeB3.Margin = new System.Windows.Forms.Padding(4);
            this.btnKodeB3.Name = "btnKodeB3";
            this.btnKodeB3.Size = new System.Drawing.Size(134, 28);
            this.btnKodeB3.TabIndex = 46;
            this.btnKodeB3.Text = "Cetak Kode B3";
            this.btnKodeB3.UseVisualStyleBackColor = true;
            this.btnKodeB3.Click += new System.EventHandler(this.btnKodeB3_Click);
            // 
            // ckAjudan
            // 
            this.ckAjudan.AutoSize = true;
            this.ckAjudan.Location = new System.Drawing.Point(403, 56);
            this.ckAjudan.Name = "ckAjudan";
            this.ckAjudan.Size = new System.Drawing.Size(265, 21);
            this.ckAjudan.TabIndex = 47;
            this.ckAjudan.Text = "Cek Jika Data Peserta adalah Ajudan";
            this.ckAjudan.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(989, 86);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(134, 28);
            this.button2.TabIndex = 48;
            this.button2.Text = "Cetak Kode B4";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // FormParticipantVIPKontingen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1440, 782);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ckAjudan);
            this.Controls.Add(this.btnKodeB3);
            this.Controls.Add(this.btnKodeB2);
            this.Controls.Add(this.btnKodeA2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbjabatan);
            this.Controls.Add(this.btnCetakVIP);
            this.Controls.Add(this.btnCetakB);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCetakA);
            this.Controls.Add(this.cmb_kontingen);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FormParticipantVIPKontingen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormParticipant";
            this.Load += new System.EventHandler(this.FormParticipant_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCetakA;
        private System.Windows.Forms.ComboBox cmb_kontingen;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewImageColumn Foto;
        private System.Windows.Forms.DataGridViewImageColumn QRCode;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnCetakB;
        private System.Windows.Forms.Button btnCetakVIP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbjabatan;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnKodeA2;
        private System.Windows.Forms.Button btnKodeB2;
        private System.Windows.Forms.Button btnKodeB3;
        private System.Windows.Forms.CheckBox ckAjudan;
        private System.Windows.Forms.Button button2;
    }
}