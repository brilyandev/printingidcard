﻿
namespace PrintingIDCard
{
    partial class FormParticipantSDM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Foto = new System.Windows.Forms.DataGridViewImageColumn();
            this.QRCode = new System.Windows.Forms.DataGridViewImageColumn();
            this.btnCetakVIP = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbBidang = new System.Windows.Forms.ComboBox();
            this.btnCetakA = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbCluster = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(167, 81);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 29;
            this.button1.Text = "Tampilkan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Foto,
            this.QRCode});
            this.dataGridView1.Location = new System.Drawing.Point(13, 129);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 80;
            this.dataGridView1.Size = new System.Drawing.Size(1414, 605);
            this.dataGridView1.TabIndex = 30;
            // 
            // Foto
            // 
            this.Foto.DataPropertyName = "Foto";
            this.Foto.HeaderText = "Foto";
            this.Foto.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Foto.MinimumWidth = 6;
            this.Foto.Name = "Foto";
            this.Foto.ReadOnly = true;
            this.Foto.Width = 125;
            // 
            // QRCode
            // 
            this.QRCode.DataPropertyName = "QRCode";
            this.QRCode.HeaderText = "QRCode";
            this.QRCode.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.QRCode.MinimumWidth = 6;
            this.QRCode.Name = "QRCode";
            this.QRCode.ReadOnly = true;
            this.QRCode.Width = 125;
            // 
            // btnCetakVIP
            // 
            this.btnCetakVIP.Location = new System.Drawing.Point(430, 81);
            this.btnCetakVIP.Margin = new System.Windows.Forms.Padding(4);
            this.btnCetakVIP.Name = "btnCetakVIP";
            this.btnCetakVIP.Size = new System.Drawing.Size(147, 28);
            this.btnCetakVIP.TabIndex = 34;
            this.btnCetakVIP.Text = "Cetak Kode SDM2";
            this.btnCetakVIP.UseVisualStyleBackColor = true;
            this.btnCetakVIP.Click += new System.EventHandler(this.btnCetakVIP_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 36;
            this.label2.Text = "Pilih Bidang";
            // 
            // cmbBidang
            // 
            this.cmbBidang.FormattingEnabled = true;
            this.cmbBidang.Items.AddRange(new object[] {
            "DPD RI",
            "DPR RI",
            "BPK RI",
            "MPR RI",
            "MK",
            "Kejagung"});
            this.cmbBidang.Location = new System.Drawing.Point(167, 17);
            this.cmbBidang.Margin = new System.Windows.Forms.Padding(4);
            this.cmbBidang.Name = "cmbBidang";
            this.cmbBidang.Size = new System.Drawing.Size(228, 24);
            this.cmbBidang.TabIndex = 35;
            this.cmbBidang.SelectedIndexChanged += new System.EventHandler(this.cmbBidang_SelectedIndexChanged);
            // 
            // btnCetakA
            // 
            this.btnCetakA.Location = new System.Drawing.Point(585, 81);
            this.btnCetakA.Margin = new System.Windows.Forms.Padding(4);
            this.btnCetakA.Name = "btnCetakA";
            this.btnCetakA.Size = new System.Drawing.Size(147, 28);
            this.btnCetakA.TabIndex = 37;
            this.btnCetakA.Text = "Cetak Kode SDM3";
            this.btnCetakA.UseVisualStyleBackColor = true;
            this.btnCetakA.Click += new System.EventHandler(this.btnCetakA_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(930, 81);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(171, 28);
            this.button4.TabIndex = 42;
            this.button4.Text = "Reset QR Code";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(751, 81);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(171, 28);
            this.button3.TabIndex = 41;
            this.button3.Text = "Generate QR Code";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(275, 81);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(147, 28);
            this.button5.TabIndex = 50;
            this.button5.Text = "Cetak Kode SDM1";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 52);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 52;
            this.label1.Text = "Pilih Cluster";
            // 
            // cmbCluster
            // 
            this.cmbCluster.FormattingEnabled = true;
            this.cmbCluster.Items.AddRange(new object[] {
            "Kota Jayapura",
            "Kabupaten Jayapura",
            "Kabupaten Mimika",
            "Kabupaten Merauke"});
            this.cmbCluster.Location = new System.Drawing.Point(167, 49);
            this.cmbCluster.Margin = new System.Windows.Forms.Padding(4);
            this.cmbCluster.Name = "cmbCluster";
            this.cmbCluster.Size = new System.Drawing.Size(228, 24);
            this.cmbCluster.TabIndex = 51;
            // 
            // FormParticipantSDM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1440, 782);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCluster);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnCetakA);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbBidang);
            this.Controls.Add(this.btnCetakVIP);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FormParticipantSDM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormParticipant";
            this.Load += new System.EventHandler(this.FormParticipant_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewImageColumn Foto;
        private System.Windows.Forms.DataGridViewImageColumn QRCode;
        private System.Windows.Forms.Button btnCetakVIP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbBidang;
        private System.Windows.Forms.Button btnCetakA;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbCluster;
    }
}