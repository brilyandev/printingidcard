﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Net;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace PrintingIDCard
{
    public partial class FormPreviewPrintBroadcast : Form
    {
        public FormPreviewPrintBroadcast()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        public DataSet dsReport;
        public string photo = "";
        public string QRCode = "";
        public Boolean Ajudan = false;
        
        private void btnPrint_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable("Atlit");
            dt.Clear();
            dt.Columns.Add("NamaLengkap");
            dt.Columns.Add("KodeAkses");
            dt.Columns.Add("Kontingen");
            dt.Columns.Add("Cabor");
            dt.Columns.Add("Cabor2");
            dt.Columns.Add("Cabor3");
            dt.Columns.Add("KodeCabor");
            dt.Columns.Add("KodeCabor2");
            dt.Columns.Add("KodeCabor3");
            dt.Columns.Add("KodeParticipant");
            dt.Columns.Add("JenisPeserta");
            DataRow _ravi = dt.NewRow();
            _ravi["NamaLengkap"] = lblfullname.Text;
            _ravi["KodeAkses"] = lblKodeAkses.Text;
            if (Ajudan)
            {
                _ravi["KodeAkses"] = "Pa";
            }
            else
            {
                if (lblKodeAkses.Text == "BROADCAST1")
                {
                    _ravi["KodeAkses"] = "RT";
                }
                if (lblKodeAkses.Text == "BROADCAST2")
                {
                    _ravi["KodeAkses"] = "RT";
                }
            }            
            _ravi["Kontingen"] = lblcontingent_name.Text;
            _ravi["Cabor"] = lblevent_name.Text;
            _ravi["KodeCabor"] = lblevent_code.Text;
            _ravi["KodeParticipant"] = lblKodeParticipant.Text ;
            _ravi["JenisPeserta"] = lblJenisPeserta.Text;
            dt.Rows.Add(_ravi);
            DataSet dsReport = new DataSet();
            dsReport.Tables.Add(dt);

            AddImageColumn(dsReport.Tables["Atlit"], "Foto");
            AddImageColumn(dsReport.Tables["Atlit"], "QRCode");
            for (int index = 0; index < dsReport.Tables[0].Rows.Count; index++)
            {
                LoadImage(dsReport.Tables[0].Rows[index], "Foto", "https://workforcevolume.oss-ap-southeast-5.aliyuncs.com" + photo);
                LoadImage(dsReport.Tables[0].Rows[index], "QRCode", PublicData.url+"/qr/" + QRCode);
            }

            PreviewPrintCdM frm = new PreviewPrintCdM();
            frm.Live = "Live";
            frm.dsReportLive = dsReport;
            frm.KodeAkses = lblKodeAkses.Text;
            frm.ShowDialog();

            //ReportDocument rd = new ReportDocument();
            //string Lokasi = Directory.GetCurrentDirectory() + "\\templatePON1.rpt";
            //rd.Load(Lokasi);
            //rd.SetDataSource(dsReport);
            //rd.PrintToPrinter(1, true, 0, 0);
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadImage(DataRow objDataRow, string strImageField, string Foto)
        {
            try
            {
                System.Net.WebClient web = new System.Net.WebClient();
                byte[] Image = null;
                Image = web.DownloadData(Foto);
                objDataRow[strImageField] = Image;
            }
            catch
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void AddImageColumn(DataTable objDataTable, string strFieldName)
        {
            try
            {
                DataColumn objDataColumn = new DataColumn(strFieldName, Type.GetType("System.Byte[]"));
                objDataTable.Columns.Add(objDataColumn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void btnUpdatPrint_Click(object sender, EventArgs e)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create("https://api.ponxx2021papua.com/workforce/registrationservice/registration/update_printing/" + lblParticipantID.Text);
                request.Method = "PUT";
                request.Headers.Add("x-access-token", PublicData.token);
                request.ContentType = "application/x-www-form-urlencoded";
                using (var streamWriter = new System.IO.StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write("printing_status=true", System.Text.Encoding.UTF8);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var response = (HttpWebResponse)request.GetResponse();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
                {
                    JObject Data = JObject.Parse(reader.ReadToEnd());
                    string dataResponse = "";
                    try
                    {
                        dataResponse = ((JValue)Data["printing_status"]).ToString();
                        MessageBox.Show(dataResponse);
                    }
                    catch
                    {
                        MessageBox.Show("Username dan password tidak tersedia");
                    }

                }
            }
            catch
            {
                MessageBox.Show("Silahkan periksa koneksi internet kamu");
            }
        }

        private void btnResetStatus_Click(object sender, EventArgs e)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create("https://api.ponxx2021papua.com/workforce/registrationservice/registration/update_printing/" + lblParticipantID.Text);
                request.Method = "PUT";
                request.Headers.Add("x-access-token", PublicData.token);
                request.ContentType = "application/x-www-form-urlencoded";
                using (var streamWriter = new System.IO.StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write("printing_status=false", System.Text.Encoding.UTF8);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var response = (HttpWebResponse)request.GetResponse();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encode))
                {
                    JObject Data = JObject.Parse(reader.ReadToEnd());
                    string dataResponse = "";
                    try
                    {
                        dataResponse = ((JValue)Data["printing_status"]).ToString();
                        MessageBox.Show(dataResponse);
                    }
                    catch
                    {
                        MessageBox.Show("Username dan password tidak tersedia");
                    }

                }
            }
            catch
            {
                MessageBox.Show("Silahkan periksa koneksi internet kamu");
            }
        }
    }
}
